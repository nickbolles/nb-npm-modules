/**
 * Created by Nick on 11/23/2016.
 */
import * as mong from 'mongoose';
import {NBName as NBNameConstructor, isNBNameStringForm, isINBName, NBName} from './NBName';

export function NBNameSchemaType(key: any, options: any): void {
    mongoose.SchemaType.call(this, key, options, 'NBNameSchemaType');
}
NBNameSchemaType.prototype = Object.create(mongoose.SchemaType.prototype);

// `cast()` takes a parameter that can be anything. You need to
// validate the provided `val` and throw a `CastError` if you
// can't convert it.
NBNameSchemaType.prototype.cast = function(val: string | NBName): NBName {
    if (isNBNameStringForm(val) || isINBName(val)) {
        return new NBNameConstructor(val);
    }
    throw new Error('NBNameType: ' + val +
        ' is not NBNameLike');
};

namespace mongoose {
    namespace Schema {
        namespace Types {
            export let NBName = NBNameSchemaType;
        }
    }
}

// Don't forget to add `Int8` to the type registry
mongoose.Schema.Types.NBName = NBNameSchemaType;
