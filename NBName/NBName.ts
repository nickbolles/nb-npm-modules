/**
 * @module NBName
 */
/**
 * Created by Nicholas on 7/6/2016.
 */
import {isString, isObject} from '@nb/nbutils';

/**
 * Interface for NBName structure
 */
export interface INBName {
    first: String;
    last: String;
}

/**
 * Type Guard for INBName
 * @param obj
 * @returns {boolean}
 */
export function isINBName (obj: any): obj is INBName {
    return isObject(obj) && 'first' in obj && 'last' in obj;
}

const regex = new RegExp('[A-z]+ [A-z]+');
/**
 *
 * @param value
 * @returns {boolean}
 */
export function isNBNameStringForm(value: any): boolean {
    return isString(value) && value.match(regex) && value.match(regex).length > 0;
}

export class NBName extends String {
    // <editor-fold dec="Static Methods">
    /**
     * Parse a json structure to an NBName
     * @param obj
     * @returns {NBName}
     */
    public static fromJSON(obj: any): NBName {
        if (isString(obj)) {
            try {
                obj = JSON.parse(obj);
            } catch (e) {
                return new NBName(obj);
            }
        }
        return new NBName(obj.first, obj.last);
    }

    /**
     * Utility function to capitalize a string. Used by the getter and setter for first and last
     * @param str
     * @returns {string}
     */
    private static capitalize(str: string): string {
        return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
    }
    // </editor-fold>

    // <editor-fold desc="Instance Properties">
    private _first: String;
    get first(): String {
        return this._first;
    }

    set first(value: String) {
        this._first = isString(value) ? NBName.capitalize(value.trim()) : value;
    }

    private _last: String;
    get last(): String {
        return this._last;
    }

    set last(value: String) {
        this._last = isString(value) ? NBName.capitalize(value.trim()) : value;
    }

    // <editor-fold desc="String Methods">
    get value(): string {
        return this.first + (this.last ? ' ' + this.last : '');
    }
    // </editor-fold>
    // </editor-fold>

    /**
     * Constructor.
     * First can be:
     *  - string in the form of '<First Name> <Last Name>', verified by isNBStringForm
     *  - string in the form of '<First Name>', the first name will be set to the string if isNBStringForm returns false
     *  - an Object in the form of INBName
     *  - an instance of NBName
     *
     *  Inputs, in any form, should be trimmed by the setter
     * @param first {String | INBName | NBName}
     * @param last {String}
     */
    constructor(first: String | INBName | NBName = '', last?: String) {
        super();
        if (isNBName(first) || isINBName(first)) {
            this.first = first.first;
            this.last = first.last || null;
        } else if (isNBNameStringForm(first)) {
            let parts = first.split(' ');
            this.first = parts[0];
            this.last = parts[1] || null;
        } else {
            this.first = first;
            this.last = last;
        }
    };

    // <editor-fold desc="String Methods">
    public toString(): string {
        return this.value;
    }
    // </editor-fold>

    // <editor-fold desc="Object Methods">
    public toJSON(): String {
        return JSON.stringify(this.toObj());
    }

    public toObj(): any {
        return {
            first: this.first,
            last: this.last
        };
    }
    // </editor-fold>

}

/**
 * Type guard for NBName
 * @param obj
 * @returns {boolean|boolean}
 */
export function isNBName(obj: any): obj is NBName {
    return isObject(obj) && 'first' in obj && 'last' in obj;
}
