import {NBName} from './NBName';
describe('NBName', () => {
    let name: NBName;

    let testNames = [
        '\'first\', \'last\'',
        '\'first last\'',
        '{first: \'\', last: \'\'}',
        'NBName(first, last)'
    ];
    let constructors = [
        () => new NBName('john', 'smith'),
        () => new NBName(' john ', ' smith '),
        () => new NBName('john smith'),
        () => new NBName('john smith'),
        () => new NBName({first: 'john', last: 'smith'}),
        () => new NBName(constructors[0]())
    ];

    constructors.forEach((constructor: Function, i: number) => {
        describe(testNames[i], () => {
            beforeEach(() => {
                name = constructor();
            });

            it('should create an NBName', () => {
                expect(name).toEqual(jasmine.any(NBName));
            });

            it('should have the correct first and last names', () => {
                expect(name.first).toBe('John');
                expect(name.last).toBe('Smith');
            });

            it('should behave like a string', () => {
                expect('' + name).toBe('John Smith');
                expect(name.toString()).toBe('John Smith');
                expect(name.value).toBe('John Smith');
            });

            it('should serialize to object correctly', () => {
                expect(name.toObj()).toBe({first: 'John', last: 'Smith'});
            });

            it('should serialize correctly', () => {
                expect(JSON.stringify(name)).toBe('{"first":"John","last":"Smith"}');
            });
        });
    });

    it('should have last name optional', () => {
        name = new NBName('john');
        expect(name.first).toBe('John');
        expect(name.last).toBeUndefined();
        name = new NBName('john ');
        expect(name.first).toBe('John');
        expect(name.last).toBeUndefined();
    });
    it('should parse from JSON', () => {
        let name2 = NBName.fromJSON(name.toJSON());
        expect(name2).toEqual(name);
        expect(name2.toJSON()).toEqual(name.toJSON());
    });
});
