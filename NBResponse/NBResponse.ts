import {isObject, isNumber} from '@nb/nbutils';
import {statusCodes} from './ErrorMessages';
/**
 * Created by Nick on 12/22/2016.
 */

export interface Error {
    toJSON(): Object;
}

/**
 * This is basically just an alias for Error in order to let typescript know about the toJSON property
 */
export type INBError = Error;

if (!('toJSON' in Error.prototype)) {
    Object.defineProperty(Error.prototype, 'toJSON', {
        value: function (): any {
            let alt = {};

            Object.getOwnPropertyNames(this).forEach(function (key: string): void {
                alt[key] = this[key];
            }, this);

            return alt;
        },
        configurable: true,
        writable: true
    });
}

export interface INBResponse {

    /**
     * A User Facing message
     * i.e. 'Unable to login'
     */
    message: string;

    /**
     * Detailed message for the response, this could be
     *   - A technical reason for an error
     *   - Info about what was updated
     *   - etc.
     */
    details: string;

    /**
     * The payload of the request
     */
    data?: any;

    /**
     * The status code that should be used for the http response
     */
    statusCode: number;
}

export function isINBResponse(obj: any): obj is INBResponse {
    return isObject(obj) &&
        'message' in obj &&
        'details' in obj &&
        'statusCode' in obj &&
        'data' in obj &&
        isObject(obj.data) &&
        isNumber(obj.statusCode);
}

export class NBResponse {
    static JSONorDef(val: any, def: any): any {
        return val ? (val.toJSON ? val.toJSON() : val) : def;
    }

    constructor(public message: string = null, public details: string = null,
                public data: any = {}, public statusCode: number = 200) {
    }

    public toJSON(): INBResponse {
        return {
            message: NBResponse.JSONorDef(this.message, statusCodes[this.statusCode]),
            details:NBResponse.JSONorDef(this.details, ''),
            // todo: fix potential errors here
            data: NBResponse.JSONorDef(this.data, {}),
            statusCode: this.statusCode
        };
    }
}

export function isNBResponse(obj: any): obj is NBResponse {
    return isObject(obj) && obj instanceof NBResponse;
}
