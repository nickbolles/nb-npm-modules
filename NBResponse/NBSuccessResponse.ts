/**
 * Created by Nick on 12/22/2016.
 */
/**
 * @module NBSuccessResponse
 */
import {isObject} from '@nb/nbutils';
import {INBResponse, NBResponse} from './NBResponse';

export interface INBSuccessResponse extends INBResponse {
}

export class NBSuccessResponse extends NBResponse {
    constructor(public message: string = null, public details: string = null,
                public data: any = {}, public statusCode: number = 200) {
        super(message, details, {}, statusCode);
    }
}

export function isNBSuccessResponse(obj: any): obj is NBSuccessResponse {
    return isObject(obj) && obj instanceof NBSuccessResponse;
}
