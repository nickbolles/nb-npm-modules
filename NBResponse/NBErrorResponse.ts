/**
 * @module NBErrorResponse
 */
/**
 * Created by Nick on 12/7/2016.
 */
import {isObject} from '@nb/nbutils';
import {INBResponse, NBResponse} from './NBResponse';

export interface INBErrorResponse extends INBResponse {
    data?: {
        /**
         * The error itself
         */
        error?: any,
        /**
         * The cause of this error, this will build a tree that can be followed down to the root cause
         */
        cause?: INBResponse
    };
}

export class NBErrorResponse extends NBResponse {
    constructor(public message: string, public details: string,
                public err?: Error, public cause?: NBErrorResponse,
                public statusCode: number = 500) {
        super(message, details, {}, statusCode);
        let obj: any = {};
        if (cause) {
            obj.cause = cause;
        }
        if (err) {
            obj.error = err;
        }
        this.data = obj;
    }/*
    public toJSON(): INBErrorResponse {
        let obj = super.toJSON();
        // Make sure that cause and error are correctly stringified
        obj.data = {
            cause: (this.data.cause) ? JSON.stringify(this.data.cause): null,
            error: (this.data.error) ? JSON.stringify(this.data.error): null
        };
        return obj;
    }*/
}

export function isNBErrorResponse(obj: any): obj is NBErrorResponse {
    return isObject(obj) && obj instanceof NBErrorResponse;
}

export function toNBErrorResponse(err: any, message: string, details: string,
                                  cause?: NBErrorResponse, statusCode?: number): NBErrorResponse {
    return isNBErrorResponse(err) ? err : new NBErrorResponse(message, details, err, cause, statusCode);
}
