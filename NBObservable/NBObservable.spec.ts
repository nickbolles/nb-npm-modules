import {NBObservable} from './NBObservable';
import {NBObserver} from './NBObserver';
/**
 * Created by Nicholas on 8/9/2016.
 */

describe('NBObservable', () => {
    let observeable: NBObservable<any>;
    let observer: NBObserver<any>;
    let spy: jasmine.Spy;
    beforeEach(() => {
        observeable = new NBObservable();
        observer = {
            changed: function(update: any): any {
                return update;
            }
        };
        spy = spyOn(observer, 'changed').and.callThrough();
    });
    it('should init', () => {
        expect(observeable).toBeDefined();
        expect(observeable).toBe(jasmine.any(NBObservable));
    });
    it('should register observers', () => {
        observeable.subscribe(observer);
        observeable.notify();
        expect(spy).toHaveBeenCalled();
    });
    it('should unsubscribe observers', () => {
        observeable.subscribe(observer);
        observeable.unsubscribe(observer);
        observeable.notify();
        expect(spy).not.toHaveBeenCalled();
    });
});
