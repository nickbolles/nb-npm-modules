/**
 * @module NBObservable
 */
/**
 * Created by Nicholas on 8/3/2016.
 */
import {isNBObservable} from './NBObservable';
import {NBObserver} from './NBObserver';
import {NBNotifyObserver} from './NBNotifyObserver';
import {isArray, NBUtils} from '@nb/nbutils';

export class NBObservableArray<T> extends Array<T> implements NBObserver<T> {
    protected _observers: NBObserver<T>[] = [];
    private _notifyObserver: NBObserver<T>;

    constructor()
    constructor(size: number)
    constructor(...array: T[]) {
        super();
// todo: move note to documentation
        /* tslint:disable:max-line-length */
        /**
         * Typescript 2.1 introduces constructor return values substituting this
         *  in order to align with ES2015 closer.
         *  https://github.com/Microsoft/TypeScript/wiki/Breaking-Changes#generated-constructor-code-substitutes-the-return-value-of-super-calls-as-this
         *
         *  This introduces errors with extending built-in types, so we need to set the prototype explicitly
         *  https://github.com/Microsoft/TypeScript/wiki/Breaking-Changes#extending-built-ins-like-error-array-and-map-may-no-longer-work
         */
        //  Set the prototype explicitly
        NBUtils.setPrototypeOf(this, NBObservableArray.prototype);
        /* tslint:enable */

        // Check to make sure this is not being invoked with an array size
        if (!(array.length === 1 && typeof array[0] === 'number')) {
            this.push(...array);
        }
        /* Hack to try to make Babel ignore the Array constructor */
        //  let thisVar = _possibleConstructorReturn(this, null);
        //  this = thisVar;
        /* Babel assigns the default properties before any user code, which the above line qualifies as */
        // Create a changed observer for this instance to use for all notifications
        this._notifyObserver = new NBNotifyObserver(this);
    }

    /*            Add Methods           */
    public push(...items: T[]): number {
        this.subscribeToItems(items);
        let result = super.push(...items);
        this.notify();
        return result;
    }

    public unshift(...items: T[]): number {
        this.subscribeToItems(items);
        let result = super.unshift(...items);
        this.notify();
        return result;
    }

    /*          Remove Methods          */
    public pop(): T {
        let val = super.pop();
        this.unsubscribeFromItems(val);
        this.notify();
        return val;
    }

    public shift(): T {
        let val = super.shift();
        this.unsubscribeFromItems(val);
        this.notify();
        return val;
    }

    /*         Add/Remove Methods        */
    public splice(start: number, deleteCount?: number, ...items: T[]): T[] {
        if (start < 0) {
            start = this.length - start - 1;
        } else if (start > this.length - 1) {
            start = this.length - 1;
        }

        this.subscribeToItems(items);

        for (let i = 0; i < deleteCount; i++) {
            let val = this[start + i];
            this.unsubscribeFromItems(val);
        }
        let result = super.splice(start, deleteCount, ...items);
        this.notify();
        return result;
    }

    /*         Array Modifiers        */
    public sort(compareFn?: (a: T, b: T) => number): this {
        super.sort(compareFn);
        this.notify();
        return this;
    }

    public reverse(): this {
        super.reverse();
        this.notify();
        return this;
    }

    public forEach(callbackfn: (value: T, index: number, array: T[]) => void, thisArg?: any): void {
        let result = super.forEach(callbackfn, thisArg);
        this.notify();
        return result;
    }

    /*  Derived From Instance Methods   */
    //  public map<U>(callbackfn: (value: T, index: number, array: T[])=>U, thisArg?: any): U[] {
    //      return super.map(callbackfn, thisArg);
    //  }
    //
    //  public concat(...items): T[] {
    //      return new NBObservableArray(super.concat(...items));
    //  }
    //
    //  public slice(start?: number, end?: number): T[] {
    //      return new NBObservableArray(super.slice(start, end));
    //  }
    //
    //  public filter(callbackfn: (value: T, index: number, array: T[])=>boolean, thisArg?: any): T[] {
    //      return new NBObservableArray(super.filter(callbackfn, thisArg));
    //  }

    public toNativeArray(): T[] {
        return this.slice(0);
    }

    public toJSON(): T[] {
        return this.toNativeArray();
    }

    public subscribe(observer: NBObserver<T>): any {
        this._observers.push(observer);
        return () => {
            this.unsubscribe(observer);
        };
    }

    public unsubscribe(observer: NBObserver<T>): void {
        this._observers.indexOf(observer);
    }

    public notify(): void {
        this._observers.forEach((observer: NBObserver<T>) => {
            observer.changed(this);
        });
    }

    public changed<T>(update: T): void {
        this.notify();
    }

    protected subscribeToItems(items: T | T[]): void {
        if (!isArray<T>(items)) {
            return this.subscribeToItem(items);
        }
        (items as any).forEach((value: T, index: number) => {
            // If this item is observable, subscribe to it and update this whenever it is updated
            this.subscribeToItem(value);
        });
    }

    protected unsubscribeFromItems(items: T | T[]): void {
        if (!isArray<T>(items)) {
            return this.unsubscribeFromItem(items);
        }
        (items as any).forEach((value: T, index: number) => {
            // If this item is observable, subscribe to it and update this whenever it is updated
            this.unsubscribeFromItem(value);
        });
    }

    private subscribeToItem(item: any): void {
        if (isNBObservable(item)) {
            (item as any).subscribe(this._notifyObserver);
        }
    }

    private unsubscribeFromItem(item: any): void {
        if (isNBObservable(item)) {
            item.unsubscribe(this._notifyObserver);
        }
    }
}

export function isNBObservableArray(obj: any): obj is NBObservableArray<any> {
    return obj instanceof NBObservableArray;
}
