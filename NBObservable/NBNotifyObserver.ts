/**
 * @module NBObservable
 */
/**
 * Created by Nicholas on 8/3/2016.
 */
import {NBObserver} from './NBObserver';

export class NBNotifyObserver implements NBObserver<any> {
    constructor(private instance: any) { }
    public changed(): void {
        this.instance.changed();
    }
}
