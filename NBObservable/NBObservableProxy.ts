/**
 * @module NBObservable
 */
/**
 * Created by Nicholas on 9/15/2016.
 */
import {NBObservable} from './NBObservable';
import * as Proxy from 'harmony-proxy';

export const NBObservableProxyHandler = {
    set: function(target: NBObservable<any>, p: any, value: any, receiver: any): boolean {
        target[p] = value;
        setTimeout(() => {
            target.notify();
        });
        return true;
    }
};

export function NBObservableProxy(fn: NBObservable<any>): NBObservable<any> {
    return new Proxy(fn, NBObservableProxyHandler);
}
