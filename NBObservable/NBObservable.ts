/**
 * @module NBObservable
 */
/**
 * Created by Nicholas on 8/3/2016.
 */

import {NBObserver} from './NBObserver';
export {NBObservableArray} from './NBObservableArray';

export class NBObservable<T> {
    protected _observers: NBObserver<T>[] = [];

    public subscribe(observer: NBObserver<T>): any {
        this._observers.push(observer);
        return () => {
            this.unsubscribe(observer);
        };
    }

    public unsubscribe(observer: NBObserver<T>): void {
        this._observers.splice(this._observers.indexOf(observer), 1);
    }

    public notify(): void {
        this._observers.forEach((observer: NBObserver<T>): void => {
            observer.changed(this);
        });
    }
}

export function isNBObservable<T>(obj: any): obj is NBObservable<T> {
    return obj && 'subscribe' in obj && 'unsubscribe' in obj && 'changed' in obj;
}
