import {NBObservable} from './NBObservable';
import {NBObserver} from './NBObserver';
import {NBObservableArray} from './NBObservableArray';
/**
 * Created by Nicholas on 8/9/2016.
 */
///<reference path="../../../../typings/globals/jasmine/index.d.ts"/>
/*
describe('NBObservableArray', () => {
    let observableArray: NBObservableArray<any>;
    let observableObj: NBObservable<any>;
    let observableObj2: NBObservable<any>;
    let observer: NBObserver<any>;
    let changedSpy: jasmine.Spy;
    let objSubscribeSpy: jasmine.Spy;
    let objUnsubscribeSpy: jasmine.Spy;
    let obj2SubscribeSpy: jasmine.Spy;
    beforeEach(() => {
        observableArray = new NBObservableArray();
        observableObj = new NBObservable();
        observableObj2 = new NBObservable();
        observer = {
            changed: function(update: any) {
                return update
            }
        };
        changedSpy = spyOn(observer, 'changed').and.callThrough();
        objSubscribeSpy = spyOn(observableObj, 'subscribe').and.callThrough();
        objUnsubscribeSpy = spyOn(observableObj, 'unsubscribe').and.callThrough();
        obj2SubscribeSpy = spyOn(observableObj2, 'subscribe').and.callThrough();
    });
    it('should init', () => {
        expect(observableArray).toBeDefined();
        expect(observableArray).toBe(jasmine.any(NBObservable));
    });
    it('should init with items', () => {
        observableArray = new NBObservableArray(0,1);
        let nativeArray = [0,1];
        expect(observableArray).toBeDefined();
        expect(observableArray).toBe(jasmine.any(NBObservable));
        expect(observableArray.length).toBe(nativeArray.length);
    });
    it('should register observers', () => {
        observableArray.subscribe(observer);
        observableArray.notify();
        expect(changedSpy).toHaveBeenCalled()
    });
    it('should unsubscribe observers', () => {
        observableArray.subscribe(observer);
        observableArray.unsubscribe(observer);
        observableArray.notify();
        expect(changedSpy).not.toHaveBeenCalled()
    });
    describe('should be an array', () => {
        let nativeArray: any[];
        beforeEach(() => {
           nativeArray = [];
        });
        describe('push', () => {
            it('should have a push method', () => {
                expect(observableArray.push).toBe(jasmine.any(Function))
            });
            it('should add element to array', () => {
                nativeArray.push(5);
                observableArray.push(5);
                expect(observableArray.length).toEqual(1);
                expect(observableArray[0]).toBe(nativeArray[0]);
            });
            it('should add multiple elements to array', () => {
                nativeArray.push(5, 4, 3, 2);
                observableArray.push(5, 4, 3, 2);
                expect(observableArray[0]).toBe(nativeArray[0]);
                expect(observableArray[1]).toBe(nativeArray[1]);
                expect(observableArray[2]).toBe(nativeArray[2]);
                expect(observableArray[3]).toBe(nativeArray[3]);
            });
            it('should add element to the end of the array', () => {
                observableArray.push(5);
                observableArray.push(4);
                observableArray.push(3);
                nativeArray.push(5);
                nativeArray.push(4);
                nativeArray.push(3);
                expect(observableArray.length).toEqual(nativeArray.length);
                expect(observableArray[0]).toBe(nativeArray[0]);
                expect(observableArray[1]).toBe(nativeArray[1]);
                expect(observableArray[2]).toBe(nativeArray[2]);
            });
            it('should return the same as a native array', () => {
                expect(observableArray.push(5)).toEqual(nativeArray.push(5));
            });
            describe('with observable extensions', () => {
                it('should notify when pushing', () => {
                    observableArray.push(observer);
                    expect(changedSpy).toHaveBeenCalled();
                });
                it('should subscribe to observables when pushing', () => {
                    observableArray.push(observableObj);
                    expect(objSubscribeSpy).toHaveBeenCalled();
                });
                it('should subscribe to multiple observables when pushing', () => {
                    observableArray.push(observableObj, observableObj2);
                    expect(objSubscribeSpy).toHaveBeenCalled();
                    expect(obj2SubscribeSpy).toHaveBeenCalled();
                });
                it('should notify once per push call', () => {
                    observableArray.push(observableObj, observableObj2);
                    expect(changedSpy).toHaveBeenCalledTimes(1)
                });
                it('should notify when pushed objects notify', () => {
                    observableArray.push(observableObj);
                    observableObj.notify();
                    expect(changedSpy).toHaveBeenCalledTimes(2);
                });
            })
        });
        describe('unshift', () => {
            it('should have a unshift method', () => {
                expect(observableArray.unshift).toBe(jasmine.any(Function))
            });
            it('should add element to array', () => {
                observableArray.unshift(5);
                nativeArray.unshift(5);
                expect(observableArray.length).toEqual(1);
                expect(observableArray[0]).toEqual(nativeArray[0]);
            });
            it('should add multiple elements to array', () => {
                nativeArray.unshift(5, 4, 3, 2);
                observableArray.unshift(5, 4, 3, 2);
                expect(observableArray[0]).toBe(nativeArray[0]);
                expect(observableArray[1]).toBe(nativeArray[1]);
                expect(observableArray[2]).toBe(nativeArray[2]);
                expect(observableArray[3]).toBe(nativeArray[3]);
            });
            it('should add element to the beginning of the array', () => {
                observableArray.unshift(5);
                observableArray.unshift(4);
                observableArray.unshift(3);
                nativeArray.unshift(5);
                nativeArray.unshift(4);
                nativeArray.unshift(3);
                expect(observableArray.length).toEqual(nativeArray.length);
                expect(observableArray[0]).toEqual(nativeArray[0]);
                expect(observableArray[1]).toEqual(nativeArray[1]);
                expect(observableArray[2]).toEqual(nativeArray[2]);
            });
            it('should return the same as a native array', () => {
                expect(observableArray.unshift(5)).toEqual(nativeArray.unshift(5));
            });
            describe('with observable extensions', () => {
                it('should notify when unshifting', () => {
                    observableArray.unshift(observer);
                    expect(changedSpy).toHaveBeenCalled();
                });
                it('should subscribe to observables when unshifting', () => {
                    observableArray.unshift(observableObj);
                    expect(objSubscribeSpy).toHaveBeenCalled();
                });
                it('should subscribe to multiple observables when unshifting', () => {
                    observableArray.unshift(observableObj, observableObj2);
                    expect(objSubscribeSpy).toHaveBeenCalled();
                    expect(obj2SubscribeSpy).toHaveBeenCalled();
                });
                it('should notify once per unshift call', () => {
                    observableArray.unshift(observableObj, observableObj2);
                    expect(changedSpy).toHaveBeenCalledTimes(1)
                });
                it('should notify when unshifted objects notify', () => {
                    observableArray.unshift(observableObj);
                    observableObj.notify();
                    expect(changedSpy).toHaveBeenCalledTimes(2);
                });
            });
        });
        describe('pop', () => {
            beforeEach(() => {
                nativeArray = [0, 1, 2, 3,];
                observableArray = new NBObservableArray<number>(...nativeArray)
            });
            it('should have a pop method', () => {
                expect(observableArray.pop).toBe(jasmine.any(Function))
            });
            it('should remove element from the array', () => {
                let last = observableArray[observableArray.length - 1];
                expect(observableArray.pop()).toEqual(last);
                expect(observableArray.pop()).toEqual(nativeArray.pop());
                expect(observableArray.length).toEqual(nativeArray.length);
            });
            describe('with observable extensions', () => {
                beforeEach(() => {
                    observableArray.push(observableObj);
                });
                it('should notify when popping', () => {
                    observableArray.pop();
                    expect(changedSpy).toHaveBeenCalled();
                });
                it('should unsubscribe to observables when popping', () => {
                    expect(observableArray.pop()).toBe(observableObj);
                    expect(objUnsubscribeSpy).toHaveBeenCalled();
                });
                it('should not notify when popped objects notify', () => {
                    expect(observableArray.pop()).toBe(observableObj);
                    observableObj.notify();
                    expect(changedSpy).toHaveBeenCalledTimes(1);
                });
            });
        });
        describe('shift', () => {
            beforeEach(() => {
                nativeArray = [0, 1, 2, 3,];
                observableArray = new NBObservableArray<number>(...nativeArray)
            });
            it('should have a pop method', () => {
                expect(observableArray.shift).toBe(jasmine.any(Function))
            });
            it('should remove element from the array', () => {
                let first = observableArray[0];
                expect(observableArray.shift()).toEqual(first);
                expect(observableArray.shift()).toEqual(nativeArray.shift());
                expect(observableArray.length).toEqual(nativeArray.length);
            });
            describe('with observable extensions', () => {
                beforeEach(() => {
                    observableArray.unshift(observableObj);
                });
                it('should notify when shifting', () => {
                    observableArray.shift();
                    expect(changedSpy).toHaveBeenCalled();
                });
                it('should unsubscribe to observables when shifting', () => {
                    expect(observableArray.shift()).toBe(observableObj);
                    expect(objUnsubscribeSpy).toHaveBeenCalled();
                });
                it('should not notify when shifted objects notify', () => {
                    expect(observableArray.shift()).toBe(observableObj);
                    observableObj.notify();
                    expect(changedSpy).toHaveBeenCalledTimes(1);
                });
            });
        });
        describe('sort', () => {
            beforeEach(() => {
                nativeArray = [0, 1, 2, 3,];
                observableArray = new NBObservableArray(...nativeArray)
            });
            it('should have a sort method', () => {
                expect(observableArray.sort).toBe(jasmine.any(Function))
            });
            it('sorted array should be the same instance', () => {
                let sortedNative = nativeArray.sort();
                expect(sortedNative).toBe(nativeArray);
                var sorted = observableArray.sort();
                //ensure that sorted and original are the same instance
                expect(sorted).toBe(observableArray);
            });
            it('sort the array the same', () => {
                nativeArray.sort();
                observableArray.sort();
                //Check to make sure native and observable are sorted the same
                nativeArray.forEach( (value, index, array) => {
                    expect(observableArray[index]).toEqual(value)
                });
            });
            it('sort the array the same with args', () => {
                let sortFn = function(a, b) { return b-a};
                nativeArray.sort(sortFn);
                observableArray.sort(sortFn);
                //Check to make sure native and observable are sorted the same
                nativeArray.forEach( (value, index, array) => {
                    expect(observableArray[index]).toEqual(value)
                });
            });
            describe('with observable extensions', () => {
                it('should notify when sorting', () => {
                    observableArray.sort();
                    expect(changedSpy).toHaveBeenCalled();
                });
            });
        });
        describe('reverse', () => {
            beforeEach(() => {
                nativeArray = [0, 1, 2, 3,];
                observableArray = new NBObservableArray<number>(...nativeArray)
            });
            it('should have a reverse method', () => {
                expect(observableArray.reverse).toBe(jasmine.any(Function))
            });
            it('reverse array should be the same instance', () => {
                let reversedNative = nativeArray.reverse();
                expect(reversedNative).toBe(nativeArray);
                var reversed = observableArray.reverse();
                //ensure that reversed and original are the same instance
                expect(reversed).toBe(observableArray);
            });
            it('reverse the array the same', () => {
                nativeArray.reverse();
                observableArray.reverse();
                //Check to make sure native and observable are sorted the same
                nativeArray.forEach( (value, index, array) => {
                    expect(observableArray[index]).toEqual(value)
                });
            });
            describe('with observable extensions', () => {
                it('should notify when reversing', () => {
                    observableArray.reverse();
                    expect(changedSpy).toHaveBeenCalled();
                });
            });
        });
        describe('forEach', () => {
            beforeEach(() => {
                nativeArray = [0, 1, 2, 3,];
                observableArray = new NBObservableArray<any>(...nativeArray);
            });
            it('should have a reverse method', () => {
                expect(observableArray.forEach).toBe(jasmine.any(Function))
            });
            it('iterate the array the same', () => {
                //Check to make sure native and observable are sorted the same
                nativeArray.forEach( (value, index, array) => {
                    expect(observableArray[index]).toEqual(value)
                });
                observableArray.forEach( (value, index, array) => {
                    expect(nativeArray[index]).toEqual(value)
                });
            });
            describe('with observable extensions', () => {
                it('should notify when iterating', () => {
                    observableArray.forEach((value) => {});
                    expect(changedSpy).toHaveBeenCalled();
                });
            });
        });
        // describe('map', () => {
        //     beforeEach(() => {
        //         nativeArray = [0, 1, 2, 3,];
        //         observableArray = new NBObservableArray(...nativeArray)
        //     });
        //     it('should have a map method', () => {
        //         expect(observableArray.map).toBe(jasmine.any(Function))
        //     });
        //     it('iterate the array the same', () => {
        //         nativeArray.map( (value, index, array) => {
        //             expect(observableArray[index]).toEqual(value)
        //         });
        //         observableArray.map( (value, index, array) => {
        //             expect(nativeArray[index]).toEqual(value)
        //         });
        //     });
        //     it('should return a new array instance', () => {
        //         let mapResult = observableArray.map( (value, index, array) => {
        //             return 2;
        //         });
        //         expect(mapResult).not.toBe(observableArray);
        //     });
        //     describe('with observable extensions', () => {
        //         it('should not notify when mapped', () => {
        //             observableArray.map((value) => {});
        //             expect(changedSpy).not.toHaveBeenCalled();
        //         });
        //     });
        // });
        // describe('concat', () => {
        //     beforeEach(() => {
        //         nativeArray = [0, 1, 2, 3,];
        //         observableArray = new NBObservableArray(...nativeArray)
        //     });
        //     it('should have a map method', () => {
        //         expect(observableArray.concat).toBe(jasmine.any(Function))
        //     });
        //     it('iterate the array the same', () => {
        //         let arrayResult = nativeArray.map( (value, index, array) => {
        //             expect(observableArray[index]).toEqual(value);
        //             return index;
        //         });
        //         let obArrayResult = observableArray.map( (value, index, array) => {
        //             expect(nativeArray[index]).toEqual(value);
        //             return index;
        //         });
        //         expect(obArrayResult).toEqual(arrayResult);
        //     });
        //     it('should return a new array instance', () => {
        //         let mapResult = observableArray.map( (value, index, array) => {
        //             return 2;
        //         });
        //         expect(mapResult).not.toBe(observableArray);
        //     });
        //     describe('with observable extensions', () => {
        //         it('should not notify when mapped', () => {
        //             observableArray.map((value) => {});
        //             expect(changedSpy).not.toHaveBeenCalled();
        //         });
        //     });
        // });
    })
});

*/
