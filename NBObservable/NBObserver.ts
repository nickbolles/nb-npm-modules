/**
 * @module NBObservable
 */
/**
 * Created by Nicholas on 8/3/2016.
 */

export interface NBObserver<T> {
    changed<T>(update: T): void;
}

export function isNBObserver(obj: any): obj is NBObserver<any> {
    return obj && 'changed' in obj;
}
