import {ngModule} from './NBAuthentication';

export * from './NBAuthentication';
export * from './INBAuthenticationResult';

export default ngModule;