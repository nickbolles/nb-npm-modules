/**
 * @module NBAuthentication
 */
/**
 * Created by Nicholas on 8/16/2016.
 */
import {UserPOJO} from '@nb/eduitemmodel';

export interface INBAuthenticationResult {
    data: {
        user: UserPOJO
    };
    message: string;
}
