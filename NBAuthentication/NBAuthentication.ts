/**
 * @module NBAuthentication
 */
/**
 * Created by Nicholas on 8/16/2016.
 */

// todo: Create lifecycle hooks so that this can be consumed by arbitrary code
import * as angular from 'angular';
import * as Cookies from 'angular-cookies';
import {INBAuthenticationResult} from './INBAuthenticationResult';
// import {NBLoaderService} from '../../loader/service';
// import {NBSettingsService} from '../../../js/Services/settings';
import {NBObserver} from '@nb/nbobservable';
import {EduItemFactory, User} from '@nb/eduitemmodel';

/* tslint:disable:member-ordering */
export class NBAuthentication implements NBObserver<User> {
    private static $inject: string[] = [
        '$rootScope',
        '$q',
        '$http',
        '$cookies',
        '$filter',
        'nbLoader',
        '$settings',
        'LocationController',
        'nbToast',
        'Utils'];

    public ready: ng.IPromise<any>;

    public loggingIn: Boolean = false;
    public loggedIn: Boolean = false;
    public user: User = new User();

    private deferred: ng.IDeferred<any>;

    constructor(private $rootScope: ng.IRootScopeService,
                private $q: ng.IQService,
                private $http: ng.IHttpService,
                private $cookies: ng.cookies.ICookiesService,
                private $filter: ng.IFilterService,
                private nbLoader: any,
                private $settings: any,
                private LocationController: any,
                private nbToast: any,
                private Utils: any) {
        this.deferred = $q.defer();
        this.ready = this.deferred.promise;

        // this.$settings.ready
        $q.when()
            .then(this.loadUser.bind(this))
            .then(this.authenticateUser.bind(this))
            .catch((err: Error) => {
                // Failed to load user, so cache the default user
                // todo: make sure this is the desired action
                this.cacheInfo(this.user);
            })
            .then(() => {
                console.log(`User Loaded! ${this.user.toString()}`);
                $rootScope.$on('saveUser', this.saveUser);
                $rootScope.$on('userData:update', this.saveUser);
            })
            .finally(() => {
                this.deferred.resolve();
                this.nbLoader.hide();
            });
    }

    public changed(): void {
        return this.saveUser();
    }

    // todo: add typedef to promise
    public authenticateUser(user: User): ng.IPromise<any> {
        // todo: Implement this function
            // todo: convert to its own function
            // todo: Determine if User needs to verify password
            // One route could be
            // if, online and over timeout then login, otherwise authenticate, but try login to refresh timeout
            return this.onLoggedIn({data: { user: user }});
            /*return this.login(this.user.email, this.user.pass).then(
                (data) => data,
                //fail
                (data) => {
                    this.nbToast.addToast('Could Not Auto Login... ' + data.message);
                    console.log('Failed to login!');
                    return data;
                }
            );*/
    }

    public login(email: string, pass: string, getData?: boolean): ng.IPromise<INBAuthenticationResult> {
        if (!email || !pass) {
            return this.$q.reject({message: 'Please Enter All Information'});
        }

        this.nbLoader.show('  Logging In...');
        this.loggingIn = true;
        let data = {
            email: email,
            pass: pass,
            appId: this.$settings.get('appId'),
            appName: this.$settings.get('appName'),
            getData: true,
            genDefaultData: this.$settings.modeSettings().genDefaultData
        };
        return this.$http({
            method: 'POST',
            url: this.$settings.modeSettings().loginURL,
            params: data,
            // set the headers so angular passing info as form data (not request payload)
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(this.onLoggedIn.bind(this), this.onRequestFail.bind(this));
    }

    public register(user: {email: string,
                            pass: string,
                            firstName: string,
                            uuid: string}): ng.IPromise<INBAuthenticationResult> {
        this.nbLoader.show('Setting Up Your Account...');
        if (!user.email || !user.pass || !user.firstName || !user.uuid) {
            return this.onRequestFail({data: {message: 'Please Enter All Information'}});
        }
        let requestData = angular.copy(user, {}) as any;
        requestData.appName = this.$settings.get('appName');
        requestData.appId = this.$settings.get('appId');
        requestData.genDefaultData = this.$settings.modeSettings().genDefaultData;
        return this.$http({
            method: 'POST',
            url: this.$settings.modeSettings().registerURL,
            params: user,  // pass in data as strings
            // set the headers so angular passing info as form data (not request payload)
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
            .then(this.onLoggedIn.bind(this), this.onRequestFail.bind(this));
    }

    private onLoggedIn(response?: any): ng.IPromise<INBAuthenticationResult> {
        let tempUser = this.user;
        this.clearCachedInfo();
        // todo: This is redundant when logging in a cached user
        this.user = response && response.data && response.data.user ?
            EduItemFactory.fromObj(response.data.user) as User :
            tempUser;
        this.cacheInfo(this.user);
        this.loggedIn = true;
        this.loggingIn = false;
        this.nbLoader.hide();
        this.nbLoader.show('Logged In! Initializing...');
        this.nbToast.addToast((response && response.message) ?
            (this.$filter('capitalize') as any)(response.message) :
            this.welcomeMessage());
        this.$rootScope.$broadcast('loggedIn');
        this.LocationController.skip();

        return this.$q.when(response);
    }

    private onRequestFail(response: any): ng.IPromise<any> {
        this.nbLoader.hide();
        this.loggedIn = false;
        this.loggingIn = false;
        let failMessage = {message: 'Unable To Contact server. Please Check your Connection and try again'};
        return this.$q.reject( response && response.data ? response.data : failMessage);
    }

    public logout(): void {
        // todo: fix $rootscope definition
        (this.$rootScope as any).$state.go('start');
        // todo: this should be done in the start controller onEnter
        this.LocationController.resetHistory();
        this.nbToast.addToast('Logged Out');
    }

    private cacheInfo(user: User): void {
        this.$cookies.put('email', user.email);
        // todo saving the hashed password is bad...
        this.$cookies.put('pass', user.pass);
        user.lastLoggedIn = this.Utils.getTimestamp();
        this.saveUser();
        this.user.subscribe(this);
    }

    private clearCachedInfo(): void {
        this.user.unsubscribe(this);
        this.user = null;
        this.$cookies.remove('email');
        // todo saving the hashed password is bad...
        this.$cookies.remove('pass');
        localStorage.removeItem('user');
        this.loggedIn = false;
    }

    public saveUser(): void {
        localStorage.setItem('user', JSON.stringify(this.user.toJSON()));
    }

    public loadUser(): ng.IPromise<User> {
        let storedUser = localStorage.getItem('user');
        if (storedUser) {
            return this.$q.when(EduItemFactory.fromJSON(storedUser) as User);
        }
        return this.$q.reject({message: 'No Stored User found'});
    }

    public welcomeMessage(): string {
        return `Hello, ${this.user.name.toString()}`;
    }
}

export let ngModule = angular.module('app.modules.nbCommon.NBAuthentication', [])
    .service('NBAuth', NBAuthentication as any);
