/**
 * @module NBUtils
 */
/**
 * Created by Nick on 12/31/2016.
 */
/// <reference path="setPrototypeOf.d.ts"/>

import * as moment from 'moment';
import {isString, isNumber, isFunction, isUndefined} from './NBTypeGuards';
import {copy} from './angular#copy';
import setPrototypeOf = require('setprototypeof');
export interface IsetPrototypeOf{
    (obj: any, proto: any): void;
}

let _setPrototypeOf: IsetPrototypeOf = (obj: any, proto: any): void => {
    (setPrototypeOf as any)(obj, proto);
}
// setPrototypeOf()

// todo: export each function separately
export let NBUtils = {
    hasKeys:             hasKeys,
    arrayObjectIndexOf:  arrayObjectIndexOf,
    getDeepProperty:     getDeepProperty,
    incrementObjVals:    incrementObjVals,
    // todo: move this to an angular utils module
    /*nextTick:            nextTick,*/
    stackTrace:          stackTrace,
    toStr:               toStr,
    toObj:               toObj,
    percentage:          percentage,
    getTimestamp:        getTimestamp,
    isObject: isObject,
    copy: copy,
    setPrototypeOf: _setPrototypeOf
};


/**
 * Count how many keys that obj1 has that are in obj2
 * This is useful for seeing if obj1 is a certain data type
 * @param obj1 the object to check
 * @param obj2 the object with the keys to check obj1 for
 */
// todo: test with arrays
function hasKeys (obj1: any, obj2: any): number {
    let count = 0;
    for (let key in obj2) {
        if (obj1.hasOwnProperty(key)) {
            count++;
        }
    }
    return count;
}

/**
 * Find the index of an item with the specified property in the array. This uses @link{getDeepProperty} for each
 * object on the Array, meaning that the property can be a period deliminated set of nested objects.
 * @param array
 * @param property
 * @param value
 * @param isCaseSens
 * @returns {number}    The index, or key of the item that has a matching property and value
 */
function arrayObjectIndexOf (array: any[] = [], property: string, value: any, isCaseSens: boolean = false): number {
    if (!isCaseSens && isString(value)) {
        value = value.toLowerCase();
    }
    let propVal;
    let result = -1;
    array.some(function(val: any, key: number): boolean{
        propVal = getDeepProperty(val, property);
        if (!isCaseSens && isString(propVal)) {
            propVal = propVal.toLowerCase();
        }
        if (propVal === value) {
            result = key;
            return true;
        }
        return false;
    });
    return result;
}

/**
 * Get the value of a property that is nested in several objects given a string of properties in the form
 *  'foo.bar.deepProp.reallyDeepProp' would return the value 'whew that was deep' from the object
 *  { foo: {
 *     bar: {
 *      deepProp:{
 *        reallyDeepProp: 'whew that was deep'
 *      }
 *    }
 *  }
 *
 * @param obj
 * @param property
 * @returns {*}
 */
function getDeepProperty (obj: any, property: string): any {
    if (obj[property]) {
        return obj[property];
    }

    let properties = property.split('.');
    let tempObj    = obj;
    // todo: catch error or throw it?
    try {
        // Loop through all but the last property
        for (let i = 0; i < properties.length - 1; i++) {
            if (isObject(tempObj[properties[i]])) {
                tempObj = tempObj[properties[i]];
            } else {
                throw new Error('\'' + properties[i] + '\' is not defined, or is not an object property of object ' +
                    tempObj);
            }
        }
        // return the final property
        return tempObj[properties[properties.length - 1]];
    } catch (e) {
        console.log('Error: Trying to get property of a non-object value', e);
        return null;
    }
}

/**
 * Increment the value of a key in obj1 by the amount of a key in obj2
 * This function modifies the original obj1 and DOES NOT return a copy of the object
 * @param obj1
 * @param obj2
 */
function incrementObjVals (obj1: any, obj2: any): void {
    for (let key in obj1) {
        if (obj1.hasOwnProperty(key) && obj2.hasOwnProperty(key) &&
            isNumber(obj1[key]) && isNumber(obj2[key])) {
            obj1[key] += obj2[key];
        }
    }
}

/*
function nextTick<T>(fn: () => T | T): ng.IPromise<T> {
    let func = isFunction(fn) ? fn : () => fn; // fn || () => fn would not work for some reason...
    let defer = $q.defer();
    $timeout(function(func){
        let result = func();
        defer.resolve(result);
    }.bind(null,func),1);
    return defer.promise;
}
*/

/**
 * Return the stacktrace of the current call
 * @returns {*}
 */
function stackTrace (): void {
    // todo: implement a better solution
    console.trace();
}

/**
 * A smarter JSON.stringify that doesn't stringify the object unless it needs to be
 * @param input
 * @param formatted
 * @returns {*}
 */
function toStr (input: any, formatted: boolean): string {
    let spacing = formatted ? 3 : 0;
    if (isString(input)) {
        return input;
    }
    // input is not a string, check to see if its an object, if it is stringify it
    if (isObject(input)) {
        return JSON.stringify(input, null, spacing);
    }
    // todo: throw error if its not an object?
}
/**
 * A smarter JSON.parse. It only parses the object if it needs to
 * @param input
 * @returns {*}
 */
function toObj (input: string): any {
    if (isObject(input)) {
        return input;
    }
    // input is not an object, check to see if its a string, if it is parse it
    if (isString(input)) {
        // if (testing){console.log('input is a string, parsing it...');};
        return JSON.parse(input);
    }

    // todo: throw error if its not an object?
    // input is not a string, log an error and return false
    // if (testing){console.log('ERROR: user data is not a string...');};
}

/**
 * Get the percentage of scored out of total
 * @param score
 * @param total
 * @returns {Number} A float
 */
function percentage(score: number, total: number): number {
    if (isUndefined(score) || score === 0) {
        return 0;
    } else if (isUndefined(total) || total === 0) {
        // Set the total equal to one because if the score is > 1 the % should be 100%+
        return 1;
    } else {
        return parseFloat(((score / total) * 100).toPrecision(12));
    }
}

function getTimestamp(): number {
    return moment.utc().unix();
}

// Invokes interceptor with the obj, and then returns obj.
// The primary purpose of this method is to "tap into" a method chain, in
// order to perform operations on intermediate results within the chain.
function tap<T>(obj: T, interceptor: (t: T) => {}): T {
    interceptor(obj);
    return obj;
}

function isObject (value: any): boolean { // Copied from Angular@1.4.9
    return value !== null && typeof value === 'object';
}
