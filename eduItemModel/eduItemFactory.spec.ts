import {EduItemFactory} from './eduItemFactory';
import * as moment from 'moment';
import {User} from './eduItem/user/user';
/**
 * Created by Nicholas on 7/6/2016.
 */
const defaultUserObj = {
    email: '',
    name: {
        first: '',
        last: ''
    },
    pass: '',
    title: '',
    completed: false,
    array: [],
    groups: [],
    tags: []
};
const userObj = {
    email: 'example@example.com',
    name: {
        first: 'firstFoo',
        last: 'lastBar'
    },
    pass: 'someFoolongBarstringFoowithBarrandomFoocharecters',
    id: '0C62D281-4C90-4505-AE13-DE8B727E7628',
    title: 'firstFoo lastBar',
    completed: false,
    created: 1467942361,
    modified: 1467942361,
    array: [],
    groups: [],
    tags: [],
    _version: 0,
    EduItemType: 'User'
};
const userJSON = JSON.stringify(userObj);

function normalizeWhitespace(str: string): string {
    return str.replace(/\s/g, '');
}

describe('User', () => {
    let newUser = new User();
    beforeEach(() => {
        newUser = new User();
    });
    it('should create a user', () => {
        expect(newUser.getClass()).toBe(User);
    });
    it('should have default values', () => {
        for (let field in defaultUserObj) {
            if (defaultUserObj.hasOwnProperty(field)) {
                expect(defaultUserObj[field]).toEqual(newUser[field]);
            }
        }
        expect(newUser.id.length).toBeGreaterThan(0);
        expect(newUser.created).toBe(jasmine.any(moment));
        expect(newUser.modified).toBe(jasmine.any(moment));
    });
    describe('Serialization', () => {
        describe('fromObj', () => {
            let user;
            beforeEach(() => {
                user = EduItemFactory.fromObj(userObj);
            });
            it('should be created', () => {
                expect(user.getClass()).toBe(User);
            });
            it('should be have correct values', () => {
                let partialUserObj = userObj;
                delete partialUserObj._version;
                delete partialUserObj.EduItemType;
                for (let field in partialUserObj) {
                    if (defaultUserObj.hasOwnProperty(field)) {
                        expect(defaultUserObj[field]).toEqual(user[field]);
                    }
                }
            });
        });
        describe('fromJSON', () => {
            let user;
            beforeEach(() => {
                user = EduItemFactory.fromJSON(userJSON);
            });
            it('should be created from JSON', () => {
                expect(user.getClass()).toBe(User);
            });
            it('should be the same input and output', () => {
                let resultObj = JSON.parse(user.toJSON());
                for (let field in userObj) {
                    if (userObj.hasOwnProperty(field)) {
                        expect(userObj[field]).toEqual(resultObj[field]);
                    }
                }
                expect(user.toObj()).toEqual(userObj);
            });
        });
    });
    describe('dates', () => {
        // todo: put this in the term unit tests
        // it('should remain null', () => {
        // let user = new User({startDate: undefined, endDate: undefined});
        // let resultObj = user.toObj();
        // expect(resultObj.startDate).toBe(undefined);
        // expect(resultObj.endDate).toBe(undefined);
        // });
    });
});
