/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 7/12/2016.
 */

import {EduItem} from '../eduItem';
import {iterateMapForProp, getUpgradeMapFromType} from './EduItemProxyUtils';
import * as Proxy from 'harmony-proxy';

function logError(target: Object, name: string | number | symbol, action: string): void {
    console.error(new Error(`Cannot ${action} property "${name} on object ${target.toString()}, it does not exist`));
}

export const EduItemUpgradeHandler = {
    get: function (target: EduItem, name: string | number /*| symbol*/, reciever: any): void {
        if (name in target) {
            return target[name];
        }
        // If the target doesn't have own property check the upgradeMap
        // todo: This does not allow for backword compatibility of values transformed by the functions
        let final = iterateMapForProp(getUpgradeMapFromType(target), target, name, null).pop();
        return final ? target[final.prop] : logError(target, name, 'get');
    },
    set: function (target: EduItem, name: string | number /*| symbol*/, value: any, reciever: any): boolean {

        // If the target doesn't have own property check the upgradeMap
        let final = iterateMapForProp(getUpgradeMapFromType(target), target, name, value).pop();
        if (final) {
            // If at least one of these is changed, otherwise the upgrade map took care of the conversion
            if (final.prop || final.val) {
                target[final.prop || name] = final.val || value;
            }
            target.notify();
            return true;
        } else if (name in target) {
            target[name] = value;
            target.notify();
            return true;
        }
        logError(target, name, 'set');
        return false;
    }
};

export function EduItemUpgradeProxy<T>(fn: T, handler?: Object): T {
    return new Proxy<T>(fn, handler || EduItemUpgradeHandler) as T;
}
