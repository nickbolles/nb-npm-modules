import {EduItem} from '../eduItem/EduItem';
import {EduItemUpgradeProxy} from './EduItemProxy';
import {User} from '../eduItem/user/user';
import ProxyHandler = harmonyProxy.ProxyHandler;
import Proxy = harmonyProxy.ProxyConstructor;
/**
 * Created by Nicholas on 7/6/2016.
 */
describe('EduItemProxy', () => {
    let eduItem: EduItem;
    let proxy: any;
    beforeEach(() => {
        eduItem = new User();
        proxy = EduItemUpgradeProxy(eduItem) as any;
    });
    it('should create a Proxy', () => {
        expect(proxy).toBe(Proxy);
    });
    it('should notify on update', () => {
        let observer = {
            changed: function (): void {
                return;
            }
        };
        let spy = spyOn(observer, 'changed');
        eduItem.subscribe(observer);
        expect(spy).not.toHaveBeenCalled();
        eduItem.label = '';
        expect(spy).toHaveBeenCalled();
    });
});
