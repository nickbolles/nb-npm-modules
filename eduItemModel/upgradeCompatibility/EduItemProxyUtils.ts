/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 8/28/2016.
 */

import {EduItem, User, Term, Course, Assign,
        UserUpgradeMap, TermUpgradeMap, CourseUpgradeMap, AssignUpgradeMap} from '../eduItem';
import {NBProxyUpgradeMap, NBProxyTransform, transformProperty as NBProxyTransformProperty,
        merge as NBProxyMerge} from '@nb/nbproxy';

export function merge<T>(dest: T, ...src: any[]): T {
    return NBProxyMerge(getUpgradeMapFromType(dest), dest, ...src);
}

export function getUpgradeMapFromType(target: EduItem | any): NBProxyUpgradeMap {
    if (target instanceof (User as any) || target.EduItemType === User.name) {
        return UserUpgradeMap;
    } else if (target instanceof (Term as any) || target.EduItemType === Term.name) {
        return TermUpgradeMap;
    } else if (target instanceof (Course as any) || target.EduItemType === Course.name) {
        return CourseUpgradeMap;
    } else if (target instanceof (Assign as any) || target.EduItemType === Assign.name) {
        return AssignUpgradeMap;
    }
}

export function iterateMapForProp(upgradeMap: NBProxyUpgradeMap, target: EduItem,
                                  name: string | number | symbol, value?: any): NBProxyTransform[] {
    return NBProxyTransformProperty(upgradeMap, target, name, value, true, true);
}
