import {EduItem} from './EduItem';

// Export the interface and class
export * from './IEduItem';
export * from './EduItem';

// Export the interface for defining a new EduItem
export * from './IEduItemTypeDef';

// Export all the sub-classes
export * from './assign';
export * from './course';
export * from './term';
export * from './user';
