/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 9/2/2016.
 */

/**
 * An EduItemType is a derived class from the base class EduItem,
 * Such as Term, Course, Assign
 * This abstract class defines the static properties of an EduItemType
 */
export class IEduItemTypeDef {

    /*         Static Properties          */
    /**
     * The name is the identifier for the EduItemType and is how it is serialized and deserialized from JSON
     *  to/from the correct class
     */
    public static name: string;
    /**
     * Display name of the EduItem Type
     * Required by subclasses of EduItem
     */
    public static dispName: string;
    /**
     * The EduItem Type that is in the array
     * Required by subclasses of EduItem
     */
    public static arrayType: IEduItemTypeDef;
    /**
     * The state for this EduItem type
     * Required to generate states for this EduItem type
     */
    public static stateName: string;

    /*        Instance Properties        */
    /**
     * The name is a built in javascript property referring to the name of the function
     * By default this will be the name of the class, so this just needs to be defined for
     * Typescript to not complain
     */
    public name: string;
}
/**
 * A Type guard for eduItemTypes
 * @param item
 * @returns {boolean}
 */
export function isIEduItemTypeDef(item: any): item is IEduItemTypeDef {
    // Check for everything but arrayType, Assign can have no arrayType
    return item && item['name'] && item['dispName'] && item['stateName'];
}
