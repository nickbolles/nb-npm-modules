import {Assign} from './assign';

// Export the interface and the class
export * from './IAssign';
export * from './assign';
// Export the upgrade map for the class
export * from './assignUpgradeHandler';

export default Assign;