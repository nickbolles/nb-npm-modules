import {Document, Schema} from 'mongoose';
import * as moment from 'moment';
import {EduItemSchemeFactory} from '../EduItem.schema';
import {Assign} from './assign';
/**
 * Created by Nick on 11/23/2016.
 */

export function AssignSchemaFactory(): any {
    let obj = EduItemSchemeFactory();
    delete obj.array;
    obj.EduItemType.default = Assign.name;
    // obj.array = {type: [Schema.Types.Mixed], default: null};
    obj.dueDateTS = {type: Number, default: () => moment.utc().valueOf()};
    obj.totalPoints = {type: Number};
    obj.scoredPoints = {type: Number};
    obj.weight = {type: Number};
    return obj;
}

export let AssignSchema = new Schema(AssignSchemaFactory());

export interface AssignDocument extends Assign, Document {
    toJSON(): any;
}
