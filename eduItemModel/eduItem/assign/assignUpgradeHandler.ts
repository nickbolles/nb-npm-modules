/**
 * The data model for EduItems
 *
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 7/28/2016.
 */

import {NBProxyUpgradeMap, NBProxyTransform} from '@nb/nbproxy';
/**
 *
 * @type {{[1.0]: {assignId}}}
 */
export const AssignUpgradeMap: NBProxyUpgradeMap = [
    {
        $$hashKey: function(target: any, value: any): NBProxyTransform {
            return {
                prop: undefined,
                val: undefined,
                message: `'$$hashkey' does not exist on updated data model, removing`
            };
        },
        _weight: function(target: any, value: any): NBProxyTransform {
            return {
                prop: undefined,
                val: undefined,
                message: `'_weight' does not exist on updated data model, removing`
            };
        },
        _id: function(target: any, value: any): NBProxyTransform {
            return {
                prop: undefined,
                val: undefined,
                message: `'_id' does not exist on updated data model, remvoing`
            };
        },
        assignName: function (target: any, value: any): NBProxyTransform {
            return {
                prop: 'label',
                val: value,
                message: `'assignName' with value ${value} converted to 'label'`
            };
        },
        sPts: function (target: any, value: any): NBProxyTransform {
            return {
                prop: 'scoredPoints',
                val: value,
                message: `'sPts' converted to 'scoredPoints' for target ${target.toString()}`
            };
        },
        tPts: function (target: any, value: any): NBProxyTransform {
            return {
                prop: 'totalPoints',
                val: value,
                message: `'tPts' converted to 'totalPoints' for target ${target.toString()}`
            };
        },
        assignId: function (target: any, value: any): NBProxyTransform {
            return {
                prop: 'id',
                val: value,
                message: `'assignId' with value ${value} converted to 'id'`
            };
        },
        dueDate: function (target: any, value: any): NBProxyTransform {
            let prop = 'dueDate';
            let message = `'dueDate' on Assign is not a number, keeping as 'dueDate' property`;
            if (typeof value === 'number') {
                prop = 'dueDateTS';
                message = `'dueDate' on Assign with value ${value} converted to 'dueDateTS'`;
            }
            return {
                prop: prop,
                val: value,
                message: message
            };
        },
        created: function (target: any, value: any): NBProxyTransform {
            let prop = 'created';
            let message = `'created' on Assign is not a number, keeping as 'created' property`;
            if (typeof value === 'number') {
                prop = 'createdTS';
                message = `'created' on Assign with value ${value} converted to 'createdTS'`;
            }
            return {
                prop: prop,
                val: value,
                message: message
            };
        },
        modified: function (target: any, value: any): NBProxyTransform {
            let prop = 'modified';
            let message = `'modified' on Assign is not a number, keeping as 'modified' property`;
            if (typeof value === 'number') {
                prop = 'modifiedTS';
                message = `'modified' on Assign with value ${value} converted to 'modifiedTS'`;
            }
            return {
                prop: prop,
                val: value,
                message: message
            };
        }
    }
];
