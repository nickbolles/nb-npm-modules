/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 9/4/2016.
 */
import {NBObservableArray} from '@nb/nbobservable';
import {EduItemPOJO, isEduItemPOJO} from '../IEduItem';
import {Assign} from './assign';

export interface AssignPOJO extends EduItemPOJO {
    array: NBObservableArray<any>;
    dueDateTS: number;
    totalPoints: number;
    scoredPoints: number;
    weight: number;
}

export function isAssignPOJO(object: any): object is AssignPOJO {
    return isEduItemPOJO(object) &&
        'dueDateTS' in object &&
        'totalPoints' in object &&
        'scoredPoints' in object &&
        'weight' in object;
}
export function isAssign(object: any): object is Assign {
    return isAssignPOJO(object) && object instanceof Assign;
}
