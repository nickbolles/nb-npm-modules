/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 6/26/2016.
 */
import * as moment from 'moment';
import {NBObservableArray} from '@nb/nbobservable';
import {isNumber} from '@nb/nbutils';
import {Grade, IGradedItem, GradeScale} from '../../grade/';
import {EduItemInsuffecientInfoError} from '../../Utils/EduItemPercentageError';
import {EduItem, IEduItemTypeDef} from '../';
import {AssignPOJO} from './IAssign';
import {Course} from '../course/course';

// todo: add a reference to parents so that we can look at how much this assignment will effect them?
export class Assign extends EduItem implements IEduItemTypeDef, IGradedItem, AssignPOJO {

    // <editor-fold desc="Static Properties">
    /**
     * The name of the class
     *
     * @description
     * This is a built in Javascript property for functions and the value will be 'Assign'
     */
    public static name: string;
    /**
     * The display name for items of this class
     *
     * @value
     * 'Assignment'
     *
     * @description
     * Used in places where the objects need to be reference to the user.
     * e.g.
     * <h1>Create an {{Assign.dispName}}</h1>
     * @type {string}
     */
    public static dispName: string = 'Assignment';
    /**
     * The type of item in the [[EduItem.array]] property
     *
     * @value
     * undefined
     *
     * @description
     * The value is undefined for Assign, as arrays don't have any child items
     */
    public static arrayType: IEduItemTypeDef;
    /**
     * The name of the state for this type of EduItem
     *
     * @value
     * 'course'
     *
     * @type {string}
     */
    public static stateName: string = 'assign';
    // </editor-fold>

    // <editor-fold desc="Instance Properties">

    // <editor-fold desc="EduItem Properties">
    /**
     * The array of sub-items for this EduItem
     *
     * @description
     * Because assigns have no child items, the array is always null
     * @type {any}
     */
    public array: NBObservableArray<any> = null;
    public EduItemType: string = Assign.name;
    // </editor-fold>

    // <editor-fold desc="IGradedItem Properties">
    // <editor-fold desc="totalPoints">
    /**
     * The points that the assignment is worth at the end of the course
     * @description
     *
     * @note: there is no predictedTotalPoints because it is a constant, the total points will be the same whether the
     * assignment is completed or not.
     * @type {number | null}
     */
    public totalPoints: number = null;

    // <editor-fold desc="completedTotalPoints">
    /**
     * The points that this assignment contributes to the current grade in the course.
     * @description
     * If this assignment is not completed then this is 0 because the current grade is only of completed assignments
     * Otherwise the entire assignment totalPoints are taken into account for the grade calculation
     *
     * This is used in conjunction with [[completedScoredPoints]]
     *
     * @note This also always returns a number, opposed to totalPoints, which can be null
     * @returns {number}
     */
    get completedTotalPoints(): number {
        return this.completed ? this.totalPoints || 0 :  0;
    }
    // </editor-fold>
    // </editor-fold>

    // <editor-fold desc="scoredPoints">
    /**
     * The scored points for this assignment.
     *
     * @description
     *
     * This can be either the predicted points, if the assignment is incomplete, or the completed points, if the
     * assignment is completed. This depends on the completed status of this
     * assignment.
     *
     * @note Getter for [[Assign.completedScoredPoints]] is available to access these values easily
     * @type {number | null}
     */
    public scoredPoints: number = null;

    /**
     * The current completed and earned points in the course
     * @description
     * The completed scored points for the assignment to count towards the current grade in the course.
     *
     * This number is used in conjunction with [[Assign.completedTotalPoints]] to calculate the current grade in
     * the course
     * @returns {number}
     */
    get completedScoredPoints(): number {
        return this.completed ? this.scoredPoints || 0 :  0;
    }
    // </editor-fold>

    // <editor-fold desc="weight">
    /**
     * The weight of this assignment in the course.
     *
     * @description
     * If this assignment has more weight then other assignments in the course this will have a weight.
     * The sum all of the assignments should not be greater then 1. If the sum of the weights is less then 1,
     * the course will distribute the rest of the weight by points.
     *
     *
     * @note: there is no predictedWeight because it is a constant, the weight will be the same whether the
     * assignment is completed or not.
     * @type {number | null}
     */
    public weight: number = null;

    /**
     * The weight to be used in calculating the current grade in the course
     *
     * @returns {number}
     */
    get completedWeight(): number {
        return this.completed ? this.weight : 0;
    }

    // <editor-fold desc="earnedWeight">
    /**
     * The weight that has been earned with this assignment.
     *
     * @description
     * Earned weight is either the predicted weight to be earned, if the assignment is incomplete,
     * or the actual earned weight from this assignment if the assignment is completed.
     *
     * Earned weight is a portion of [[Assign.weight]]. This is calculated by [[Assign.percentage]] * [[Assign.weight]]
     *
     * The earnedWeight is used to calculate the current grade in the course
     * @returns {any}
     */
    get earnedWeight(): number {
        try {
            return this.percentage * this.weight;
        } catch (e) {
            return 0;
        }
    }
    set earnedWeight(weight: number) {
        this.percentage = weight / this.weight * 100;
    }
    // </editor-fold>

    /**
     * The weight towards the current grade in the course
     *
     * @description
     * The completed earned weight is the weight that has been earned so far out of the completed weight. This
     * is used to calculate the current grade in the course with [[Assign.completedWeight]]
     *
     * The completedEarnedWeight is comparable to [[Assign.completedScoredPoints]]
     * @returns {number}
     */
    get completedEarnedWeight(): number {
        return this.completed ? this.earnedWeight : 0;
    }

    // </editor-fold>

    // <editor-fold desc="percentage">
    /**
     * The percent of points on the assignment
     *
     * @returns {number} 0-100
     */
    get percentage(): number | undefined {
        if (isNumber(this.totalPoints) && isNumber(this.scoredPoints) && this.totalPoints !== 0) {
            return this.scoredPoints / this.totalPoints * 100;
        }
        return 0;
        //  console.error(new EduItemInsuffecientInfoError());
    }

    /**
     * Sets the percent on the assignment
     *
     * @description
     * The Percent is set by setting the [[Assign.scoredPoints]] to a percentage of the [[Assign.totalPoints]], or
     * if [[Assign.totalPoints]] is not defined, setting the [[Assign.totalPoints]] to a multiple of the
     * [[Assign.scoredPoints]]
     * @param percent
     */
    set percentage(percent: number) {
        percent = Math.max(percent, 0);
        if (isNumber(this.totalPoints)) {
            this.scoredPoints = this.totalPoints * percent;
            return;
        } else if (isNumber(this.scoredPoints)) {
            this.totalPoints = this.scoredPoints / (percent || 1);
            return;
        }
        console.error(new EduItemInsuffecientInfoError());
    }

    /**
     * The minimum percent that can be earned for this assignment
     *
     * @description
     * The minimum percent is either, 0, or if the assignment is completed, it is the [[Assign.percentage]]
     *
     * @returns {number}
     */
    get minPercentage(): number {
        return this.completed ? this.percentage : 0;
    }

    /**
     * The maximum percent that can be earned for this assignment
     *
     * @description
     * The maximum percent is either, 100, or if the assignment is completed, it is the [[Assign.percentage]]
     *
     * @returns {number}
     */
    get maxPercentage(): number {
        return this.completed ? this.percentage : 100;
    }
    // </editor-fold>

    // <editor-fold desc="grade">
    /**
     * The current grade in the assignment
     *
     * @description
     * Return the [[Grade]] that corresponds to the current percentage of the assignment.
     *
     * @returns {Grade}
     */
    get grade(): Grade {
        return this.gradeScale.getGrade(this.percentage);
    }

    // </editor-fold>

    /**
     * The [[GradeScale]] to grade this assignment on
     * @type {GradeScale}
     */
    public gradeScale: GradeScale = GradeScale.defaultScale;
    // </editor-fold>

    /**
     * The due date of the assignment     *
     * @type {Moment}
     */
    public dueDate: moment.Moment = moment.utc();

    /**
     * The unix timestamp of the due date of the assignment
     * @returns {number}
     */
    // <editor-fold desc="dueDateTS">
    get dueDateTS(): number {
        return this.dueDate ? this.dueDate.valueOf() : null;
    }

    set dueDateTS(val: number) {
        if (typeof val !== 'number') {
            let mom = moment(val);
            if (moment.isMoment(mom) && mom.isValid()) {
                this.dueDate = mom;
                return;
            }
        }
        this.dueDate = val ? moment.utc(val) : null;
    }
    // </editor-fold>

    // </editor-fold>

    // <editor-fold desc="Constructor">
    constructor(obj?: any, parent?: Course) {
        super(obj);
        this.postInit(obj, parent);
    }

    // </editor-fold>

    // <editor-fold desc="Object Methods">

    /**
     * Override the EduItem toObj to extend the default object with the additional
     *  'totalPoints', 'scoredPoints', 'weight' and 'dueDate' fields
     *
     * @returns {any}
     */
    public toObj(): AssignPOJO {
        let obj = super.toObj() as AssignPOJO;
        obj.dueDateTS = this.dueDateTS;
        obj.totalPoints = this.totalPoints;
        obj.scoredPoints = this.scoredPoints;
        obj.weight = this.weight;
        return obj;
    }

    /**
     * Override the EduItem mergeFrom to copy the additional
     *   'totalPoints', 'scoredPoints', 'weight' and 'dueDateTS' fields
     * @param obj
     */
    public mergeFrom(obj: any): this {
        super.mergeFrom(obj);
        this.totalPoints = obj.totalPoints || this.totalPoints;
        this.scoredPoints = obj.scoredPoints || this.scoredPoints;
        this.weight = obj.weight || this.weight;
        this.dueDateTS = obj.dueDateTS;
        this.changed(this);
        return this;
    }

    /**
     * Override the EduItem inheritFrom to inherit the [[Assign.gradeScale]] from the parent [[Course]]
     * @param parent
     */
    protected inheritFrom(parent: Course): void {
        super.inheritFrom(parent);
        this.gradeScale = parent.gradeScale;
    }

    // </editor-fold>

}
