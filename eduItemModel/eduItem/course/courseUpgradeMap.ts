/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 7/28/2016.
 */
import {NBProxyUpgradeMap, NBProxyTransform} from '@nb/nbproxy';
import {NBObservableArray} from '@nb/nbobservable';
import {EduItemFactory} from '../../eduItemFactory';
import {EduItemPOJO} from '../IEduItem';
import {Assign} from '../assign/assign';

export const CourseUpgradeMap: NBProxyUpgradeMap = [
    {
        $$hashkey: function(target: any, value: any): NBProxyTransform {
            return {
                prop: undefined,
                val: undefined,
                message: `'$$hashkey' does not exist on updated data model, removing`
            };
        },
        _id: function(target: any, value: any): NBProxyTransform {
            return {
                prop: undefined,
                val: undefined,
                message: `'_id' does not exist on updated data model, remvoing`
            };
        },
        courseName: function (target: any, value: any): NBProxyTransform {
            return {
                prop: 'label',
                val: value,
                message: `'courseName' with value ${value} converted to 'label'`
            };
        },
        assigns: function (target: any, value: any): NBProxyTransform {
            let array = new NBObservableArray<Assign>();
            value.forEach((val: EduItemPOJO) => {
                array.push(EduItemFactory.fromObj(val) as Assign);
            });
            return {
                prop: 'array',
                val: array,
                message: `'assigns' property converted to NBObservableArray<Assign>`
            };
        },
        courseId: function (target: any, value: any): NBProxyTransform {
            return {
                prop: 'id',
                val: value,
                message: `'courseId' with value ${value} converted to 'id'`
            };
        },
        created: function (target: any, value: any): NBProxyTransform {
            let prop = 'created';
            let message = `'created' on Course is not a number, keeping as 'created' property`;
            if (typeof value === 'number') {
                prop = 'createdTS';
                message = `'created' on Course with value ${value} converted to 'createdTS'`;
            }
            return {
                prop: prop,
                val: value,
                message: message
            };
        },
        modified: function (target: any, value: any): NBProxyTransform {
            let prop = 'modified';
            let message = `'modified' on Course is not a number, keeping as 'modified' property`;
            if (typeof value === 'number') {
                prop = 'modifiedTS';
                message = `'modified' on Course with value ${value} converted to 'modifiedTS'`;
            }
            return {
                prop: prop,
                val: value,
                message: message
            };
        }
    },
    {
        array: function (target: any, value: any): NBProxyTransform {
            let array = new NBObservableArray<Assign>();
            value.forEach((val: EduItemPOJO) => {
                array.push(EduItemFactory.fromObj(val) as Assign);
            });
            return {
                prop: 'array',
                val: array,
                message: `'array' Property converted into NBObservableArray<Term> from raw array`
            };
        }
    }
];
