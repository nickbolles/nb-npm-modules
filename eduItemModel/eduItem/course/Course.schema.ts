import {Document, Schema} from 'mongoose';
import {EduItemSchemeFactory} from '../EduItem.schema';
import {AssignSchema} from '../assign/Assign.schema';
import {Course} from './course';
/**
 * Created by Nick on 11/23/2016.
 */

export function CourseSchemaFactory(): any {
    let obj = EduItemSchemeFactory();
    obj.EduItemType.default = Course.name;
    obj.array = { type: [AssignSchema], required: true, default: []};
    obj.instructor = {type: String};
    obj.credits = {type: Number, default: 3};
    obj.totalPoints = {type: Number};
    obj.scoredPoints = {type: Number};
    // todo: Save GradeScale: ICourse.ts, Course.ts, Course.schema.ts
    return obj;
}

export let CourseSchema = new Schema(CourseSchemaFactory());

export interface CourseDocument extends Course, Document {
    toJSON(): any;
}

// export const Courses = model<CourseDocument>('User', CourseSchema);
