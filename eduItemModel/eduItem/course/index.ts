import {Course} from './course';

// Export the interface and the class
export * from './ICourse';
export * from './course';
// Export the upgrade map for the Course class
export * from './courseUpgradeMap';

export default Course;