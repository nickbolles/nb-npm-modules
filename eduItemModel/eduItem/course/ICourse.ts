/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 9/4/2016.
 */
import {EduItemPOJO, isEduItemPOJO} from '../IEduItem';
import {Course} from './course';
import {Assign} from '../assign/';
import {NBObservableArray} from '@nb/nbobservable';

export interface CoursePOJO extends EduItemPOJO {
    array: NBObservableArray<Assign>;
    instructor: string;
    credits: number;
    totalPoints: number;
    scoredPoints: number;
    // todo: Save Gradescale: ICourse.ts, Course.ts, Course.schema.ts
}

export function isCoursePOJO(object: any): object is CoursePOJO {
    return isEduItemPOJO(object) &&
        'instructor' in object &&
        'credits' in object;
}
export function isCourse(object: any): object is Course {
    return isCoursePOJO(object) && object instanceof Course;
}
