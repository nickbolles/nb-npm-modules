/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 7/6/2016.
 */
import {NBObserver, NBObservableArray, NBObservableProxy} from '@nb/nbobservable';
import {isNumber} from '@nb/nbutils';
import {Grade, GradeScale, IGradedItem} from '../../grade/';
import {EduItem, isEduItem, IEduItemTypeDef} from '../';
import {CoursePOJO} from './ICourse';
import {Term} from '../term/';
import {Assign} from '../assign/';

interface IToBeDistributed {
    points: number;
    items: Assign[];
}

export class Course extends EduItem implements IEduItemTypeDef, IGradedItem, CoursePOJO,
                                                NBObserver<EduItem | NBObservableArray<EduItem>> {

    // <editor-fold desc="Static Properties">
    /**
     * The name of the class
     *
     * @description
     * This is a built in Javascript property for functions and the value will be 'Course'
     */
    public static name: string;
    /**
     * The display name for items of this class
     *
     * @value
     * 'Course'
     *
     * @description
     * Used in places where the objects need to be reference to the user.
     * e.g.
     * <h1>Create an {{Course.dispName}}</h1>
     * @type {string}
     */
    public static dispName: string = 'Course';
    /**
     * The type of item in the [[EduItem.array]] property
     *
     * @value
     * [[Assign]]
     *
     * @description
     * The value is the class [[Assign]]
     */
    public static arrayType: IEduItemTypeDef = Assign;
    /**
     * The name of the state for this type of EduItem
     *
     * @value
     * 'course'
     *
     * @type {string}
     */
    public static stateName: string = 'course';
    // </editor-fold>

    // <editor-fold desc="Static Methods">
    /**
     * Function to add the calculations from an item in the array to the calculations object
     *
     * @param toBeDistributed
     * @param calcs
     * @param assign
     * @returns {IGradedItem}
     */
    private static addItemCalcs(toBeDistributed: IToBeDistributed, calcs: IGradedItem, assign: Assign): IGradedItem {
        // While completed is a boolean it is converted to 1 or 0 because this is an arithmetic operation
        calcs.completedAssigns += Number(assign.completed);
        calcs.totalPoints += assign.totalPoints || 0;
        calcs.completedTotalPoints += assign.completedTotalPoints;
        calcs.scoredPoints += assign.scoredPoints || 0;
        calcs.completedScoredPoints += assign.completedScoredPoints;
        if (assign.weight == null) {
            toBeDistributed.items.push(assign);
            toBeDistributed.points += assign.totalPoints;
        } else {
            calcs.weight += assign.weight;
            calcs.earnedWeight += assign.earnedWeight || 0;
            calcs.completedWeight += assign.completedWeight || 0;
        }

        return calcs;
    }

    private static distributeWeightToAssign(toBeDistributed: IToBeDistributed, calcs: IGradedItem,
                                            assign: Assign): IGradedItem {
        let assignWeight = (assign.totalPoints / toBeDistributed.points) * (1 - calcs.weight);
        let earnedWeight = assignWeight * assign.percentage / 100 || 0;
        calcs.weight += assignWeight || 0;
        calcs.earnedWeight += earnedWeight;
        // todo: make completed weight and in progress weight
        if (assign.completed) {
            calcs.completedWeight += assignWeight;
            calcs.completedEarnedWeight += earnedWeight;
        }
        return calcs;
    }

    // </editor-fold>

    // <editor-fold desc="Instance Properties">

    // <editor-fold desc="EduItem Properties">
    public array: NBObservableArray<Assign> = new NBObservableArray<Assign>();
    public EduItemType: string = Course.name;
    // </editor-fold>

    //  <editor-fold desc="IGradedItem Properties">
    public assigns: number;
    public completedAssigns: number;
    public totalPoints: number;
    public completedTotalPoints: number;
    public scoredPoints: number;
    public completedScoredPoints: number;
    public weight: number;
    public completedWeight: number;
    public earnedWeight: number;
    public completedEarnedWeight: number;

    // <editor-fold desc="percentage">
    get percentage(): number {
        if (isNumber(this.totalPoints) && isNumber(this.scoredPoints) && this.totalPoints !== 0) {
            return this.scoredPoints / this.totalPoints * 100;
        }
        //  console.error(new EduItemInsuffecientInfoError());
        return 0;
    }

    get minPercentage(): number {
        return this.earnedWeight * 100;
    }

    get maxPercentage(): number {
        return 100 - (this.completedWeight - this.earnedWeight) * 100;
    }
    // </editor-fold>
    // <editor-fold desc="grade">
    get grade(): Grade {
        return this.gradeScale.getGrade(this.percentage);
    }

    // </editor-fold>
    // todo: Save Gradescale: ICourse.ts, Course.ts, Course.schema.ts
    public gradeScale: GradeScale = GradeScale.defaultScale;
    // </editor-fold>

    /**
     * The name of the instructor of the course
     *
     * @type {string}
     */
    public instructor: string = '';
    /**
     * The number of credit hours that the course is worth
     *
     * @default
     * 3
     *
     * @type {number}
     */
        // todo: Should the default number of credits be configurable?
    public credits: number = 3;

    // </editor-fold>

    // <editor-fold desc="Constructor">
    constructor(obj?: any, parent?: Term) {
        super(obj);
        this.postInit(obj, parent);
    }

    // </editor-fold>

    // <editor-fold desc="Calculation Methods">
    /**
     * Method to update the calculations for this object
     *
     * @description
     * This method loops through every item in the array and calls [[Course.addItemCalcs]], which will increment
     * the initCalcs object with the calculations.
     *
     * After this is completed each calculated property is copied to this item
     *
     * Finally, the observers of this item are notified that the calculations have changed
     */
    public updateArrayCalcs(): void {
        /*
         * //todo: Is this still applicable?
         *  Because this will change properties on the assignment, and is called from the notify of the assignment
         *  This will cause a loop if the observers aren't suspended
         */
        let initCalcs = {
            assigns: this.array.length,
            completedAssigns: 0,
            totalPoints: 0,
            completedTotalPoints: 0,
            scoredPoints: 0,
            completedScoredPoints: 0,
            weight: 0,
            completedWeight: 0,
            earnedWeight: 0,
            completedEarnedWeight: 0
        };
        let toBeDistributed = {
            points: 0,
            items: []
        };
        let calculations = this.array.reduce(Course.addItemCalcs.bind(this, toBeDistributed), initCalcs);

        //  Distribute the left over weight to the assignments distributed by % of unweighted points
        calculations = toBeDistributed.items.reduce(
            Course.distributeWeightToAssign.bind(this, toBeDistributed),
            calculations);

        for (let prop in calculations) {
            if (calculations.hasOwnProperty(prop)) {
                this[prop] = calculations[prop];
            }
        }
        this.notify();
    }

    // </editor-fold>

    // <editor-fold desc="NBObserver Methods">
    /**
     * Observer method for the Course
     *
     * @description
     * Whenever something that this Course is observing changes this method is called. This will usually be the _array
     * property.
     *
     * This method will kick off an update of the calculations by calling [[User.updateArrayCalcs]]
     * @param update
     */
    public changed<EduItem>(update: EduItem | NBObservableArray<EduItem>): void {
        this.updateArrayCalcs();
        super.changed(update);
    }
    // </editor-fold>

    // <editor-fold desc="Object Methods">

    /**
     * Override the EduItem toObj to extend the default object with the additional
     *   'instructor' and 'credits' fields
     * @returns {any}
     */
    public toObj(): CoursePOJO {
        // todo: Save Gradescale: ICourse.ts, Course.ts, Course.schema.ts
        let obj = super.toObj() as CoursePOJO;
        obj.instructor = this.instructor;
        obj.credits = this.credits;
        return obj;
    }

    /**
     * Override the EduItem mergeFrom to copy the additional
     *   'instructor', 'credits' and the array fields
     * @param obj
     */
    public mergeFrom(obj: any): this {
        super.mergeFrom(obj);
        // todo: Read Gradescale: ICourse.ts, Course.ts, Course.schema.ts
        this.instructor = obj.instructor || this.instructor;
        this.credits = obj.credits || this.credits;
        if (obj.array) {
            for (let assign of obj.array) {
                let t = assign;
                if (!isEduItem(t)) {
                    t = NBObservableProxy(new Assign(t)) as EduItem;
                }
                this.addOrMergeItem(t);
            }
        }
        this.changed(this);
        return this;
    }

    /** Inherit the completed property of the Term
     * @param parent
     */
    protected inheritFrom(parent: Term): void {
        super.inheritFrom(parent);
    }
// </editor-fold>

}
