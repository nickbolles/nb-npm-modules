/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 9/4/2016.
 */
import {EduItem} from './EduItem';
import {INBGroupArray} from '../group/index';

/**
 * An EduItem is an instance of an EduItemType,
 * such as Term, Course, Assign
 * This interface defines the needed properties of EduItems
 */
export interface EduItemPOJO {
    id: string;
    label: string;
    completed: boolean;
    createdTS: number;
    modifiedTS: number;
    // todo: convert to groupId
    groupName: string;
    array: EduItem[];
    groups: INBGroupArray;
    tags: string[];
    notes: string;
    _version: number;
    EduItemType: string;
}

export function isEduItemPOJO(object: any): object is EduItemPOJO {
    return object && typeof object === 'object' &&
        'id' in object &&
        'label' in object &&
        'completed' in object &&
        'createdTS' in object &&
        'modifiedTS' in object &&
        'groupName' in object &&
        'array' in object &&
        'groups' in object &&
        'tags' in object &&
        'notes' in object &&
        'EduItemType' in object;
}
export function isEduItem(object: any): object is EduItem {
    return isEduItemPOJO(object) && object instanceof EduItem;
}
