import {User} from './user';

// Export the interface and the class
export * from './IUser';
export * from './user';
// Export the upgrade map for the user class
export * from './userUpgradeMap';

export default User;