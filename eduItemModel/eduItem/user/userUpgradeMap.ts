/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 7/28/2016.
 */
import {Term} from '../term/term';
import {NBProxyUpgradeMap, NBProxyTransform} from '@nb/nbproxy';
import {NBObservableArray} from '@nb/nbobservable';
import {EduItemFactory} from '../../eduItemFactory';
import {EduItemPOJO} from '../IEduItem';

export const UserUpgradeMap: NBProxyUpgradeMap = [
    {
        $$hashkey: function(target: any, value: any): NBProxyTransform {
            return {
                prop: undefined,
                val: undefined,
                message: `'$$hashkey' does not exist on updated data model, removing`
            };
        },
        _id: function(target: any, value: any): NBProxyTransform {
            return {
                prop: undefined,
                val: undefined,
                message: `'_id' does not exist on updated data model, remvoing`
            };
        },
        terms: function (target: any, value: any): NBProxyTransform {
            let array = new NBObservableArray<Term>();
            if (value) {
                value.forEach((val: EduItemPOJO) => {
                    array.push(EduItemFactory.fromObj(val) as Term);
                });
            }
            return {
                prop: 'array',
                val: array,
                message: `'Terms' Property converted to NBObservableArray<Term> property 'array'`
            };
        },
        userData: function (target: any, value: any): NBProxyTransform {
            // the proxy doesn't work on nested properties, so we need to call the handler function directly.
            let arrayResult = UserUpgradeMap[0]['terms'](null, value.data.terms);
            target.array = arrayResult.val;
            target.modifiedTS = value.data.modified;
            target.completed = value.data.completed;
            target.groupName = value.data.groupName;
            target.groups = value.data.groups;
            return {
                prop: undefined,
                val: undefined,
                message: `User's 'userData' property converted to User eduItem`
            };
        }
    },
    {
        array: function (target: any, value: any): NBProxyTransform {
            let array = new NBObservableArray<Term>();
            if (value) {
                value.forEach((val: EduItemPOJO) => {
                    array.push(EduItemFactory.fromObj(val) as Term);
                });
            }
            return {
                prop: 'array',
                val: array,
                message: `'array' Property converted into NBObservableArray<Term> from raw array`
            };
        }
    }
];
