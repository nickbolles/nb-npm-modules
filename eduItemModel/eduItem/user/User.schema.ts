import * as express from 'express';
import {Document, Schema, model, Model} from 'mongoose';
import {EduItemSchemeFactory} from '../EduItem.schema';
import {TermSchema} from '../term/Term.schema';
import {NBLoginSchema} from './NBLogin.schema';
import {User} from './user';
/**
 * Created by Nick on 11/23/2016.
 */
// todo: on new users, send an email
let emailRegex = new RegExp('[A-Z0-9a-z.!#$%&\'*+-/=?^_`{|}~]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}');
let emailMatch = [
        emailRegex,
        '({VALUE}) is not a valid Email Address'
    ];
export function UserSchemaFactory(): any {
    let obj = EduItemSchemeFactory();
    obj.EduItemType.default = User.name;
    obj.array = { type: [TermSchema], required: true, default: []};
    obj.name = {
        first: {type: String, required: true},
        last: {type: String, required: false}
    };
    // todo: make this default a variable
    obj.label = {type: String, required: true, default: 'My Grades'};
    obj.email = {type: String,  required: true, unique: true, index: true,
        lowercase: true, match: emailMatch};
    // todo: is this pass needed?
    obj.pass = {type: String, required: true};
    obj.user_id = {type: Schema.Types.ObjectId, required: true, unique: true};
    obj.logins = {type: [NBLoginSchema]};

    // todo: are these needed?

    // appStats     : [{type: Schema.Types.ObjectId, ref: 'Stat'}],
    // logins       : [{type: Schema.Types.ObjectId, ref: 'Login'}],
    // feedback     : [{type: Schema.Types.ObjectId, ref: 'Feedback'}]

    return obj;
}

export let UserSchema = new Schema(UserSchemaFactory());

// UserSchema.virtual('welcome', welcomeVirtual);
// function welcomeVirtual() {
//     return this.name
// }

UserSchema.method('login', loginUser);

// todo: should this even be done in the app server?? alternative could be to just let the user server
// know how we are logging in and have it record it
function loginUser(req: express.Request, done: Function): void {
    let ipAddress = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    let login     = {
            success    : true,
            activity   : (this.logins.length === 0) ? 'Creating User' : 'Logging in...', // todo: do better
            ipAddress  : ipAddress
        };
    this.logins.push(login);
    this.save();
    done(null, this, {message: 'Authentication Successful'});
    /*this.populate('appStats', onAppStatsPop);
    function onAppStatsPop(){
        if (this.appStats.length > 0) {
            var newStat = this.appStats[this.appStats.length - 1].dup();
            newStat.increment();
            newStat.save(function (err) {
                    if (err) {
                        return done(err, false, {message: 'Error recording app stats'});
                    } else {
                        this.appStats.push(newStat._id);
                        this.save();
                        return done(null, this, {message: 'Authentication Successful'});
                    }
                }
            );
        } else {
            var newStat = new Stat({
                _GpaAppData: this._id,
                logins     : [login.id],
                launchNum  : 1,
                loginNum   : 1
            });
            newStat.save(function (err) {
                if (err) {
                    return done(err, false, {message: 'Error recording app stats'});
                }
                this.appStats.push(newStat._id);
                this.save();
                return done(null, this, {message: 'Authentication Successful'});
            });
        }
    }*/
}
export interface UserDocument extends User, Document {
    login(req: express.Request, done: Function): void;
    toJSON(): any;
}

export const Users = model<UserDocument>('User', UserSchema);
