/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 7/6/2016.
 */
import {NBObserver, NBObservableArray, NBObservableProxy} from '@nb/nbobservable';
import {NBName} from '@nb/nbname';
import {IGPAItem, GPAScale} from '../../gpa/';
import {EduItem, isEduItem, IEduItemTypeDef} from '../';
import {UserPOJO} from './IUser';
import {Term} from '../term/term';

interface IToBeDistributed {
    percentCompleteSum: number;
}

//  todo: extend GPAItem instead? would cut file in half almsot
export class User extends EduItem implements IEduItemTypeDef, IGPAItem, UserPOJO,
                                             NBObserver<EduItem | NBObservableArray<EduItem>> {

    //  <editor-fold desc="Static Properties">
    public static name: string;
    public static dispName: string = 'User';
    public static arrayType: IEduItemTypeDef = Term;
    public static stateName: string = 'home';
    //  </editor-fold>

    //  <editor-fold desc="Static Methods">
    /**
     * Function to add the calculations from an item in the array to the calculations object
     * @param toBeDistributed
     * @param calcs
     * @param term
     * @returns {IGpaItem}
     */
    private static addItemCalcs(toBeDistributed: IToBeDistributed, calcs: IGPAItem, term: Term): IGPAItem {
        calcs.assigns += term.assigns;
        calcs.completedAssigns += term.completedAssigns;
        calcs.courses += term.courses;
        calcs.completedCourses += term.completedCourses;
        calcs.credits += term.credits;
        calcs.completedCredits += term.completedCredits;
        //  Completed is a boolean but it is converted to 1 or 0 because this is an arithmetic operation
        //  Use Number() to make typescript happy
        calcs.completedTerms += Number(term.completed);
        calcs.gradePoints += term.gradePoints;
        calcs.completedGradePoints += term.completedGradePoints;
        calcs.minGradePoints += term.minGradePoints;
        calcs.maxGradePoints += term.maxGradePoints;
        calcs.totalPoints += term.totalPoints;
        calcs.minPoints += term.minPoints;
        calcs.maxPoints += term.maxPoints;
        //  Sum up a weighted average of the completed weight
        toBeDistributed.percentCompleteSum += term.percentComplete;
        return calcs;
    }
    //  </editor-fold>

    //  <editor-fold desc="Instance Properties">

    //  <editor-fold desc="EduItem Properties">
    public array: NBObservableArray<Term> = new NBObservableArray<Term>();
    public EduItemType: string = User.name;

    //  <editor-fold desc="label">
    /**
     * Override the default label to always return 'My Grades'
     * @returns {string}
     */
    //  todo: Make this a config variable along with App Name, User.schema.ts too
    get label(): string {
        return 'My Grades';
    }
    set label(val: string) {
        // Do nothing
    }
    // </editor-fold>
    // </editor-fold>

    // <editor-fold desc="IGPAItem Properties">
    public assigns: number;
    public completedAssigns: number;
    public courses: number;
    public completedCourses: number;
    public terms: number;
    public completedTerms: number;
    public credits: number;
    public completedCredits: number;
    public gradePoints: number;
    public completedGradePoints: number;
    public minGradePoints: number;
    public maxGradePoints: number;
    public totalPoints: number;
    public minPoints: number;
    public maxPoints: number;
    public percentComplete: number;

    // <editor-fold desc="gpa">

    get gpa(): number {
        return this.gradePoints / this.credits;
    }

    get minGpa(): number {
        return this.minGradePoints / this.credits;
    }

    get maxGpa(): number {
        return this.maxGradePoints / this.credits;
    }

    // </editor-fold>

    public gpaScale: GPAScale = GPAScale.defaultScale;
    // </editor-fold>

    /**
     * The internal property for the User's name
     *
     * @type {NBName}
     */
    public _name: NBName = new NBName();
    // <editor-fold desc="name">
    /**
     * Get the users name as a string representation
     *
     * @returns {string}
     */
    get name(): string {
        return this._name.toString();
    }

    /**
     * Set the users name
     *
     * @param name
     */
    set name(name: string) {
        this._name = new NBName(name);
    }

    // </editor-fold>
    /**
     * The users email address
     *
     * @type {string}
     */
    public email: string = null;
    /**
     * The users hashed password
     * @type {any}
     */
    public pass: string = null; //  todo: remove!
    /**
     * The last time the user logged in
     * @type {number}
     */
    public lastLoggedIn: number = 0;

    // </editor-fold>

    // <editor-fold desc="Constructor">
    constructor(obj?: any, parent?: EduItem) {
        super(obj);
        this.postInit(obj, parent);
    }

    // </editor-fold>

    // <editor-fold desc="Calculation Methods">
    /**
     * Method to update the calculations for this object
     *
     * @description
     * This method loops through every item in the array and calls [[User.addItemCalcs]], which will increment
     * the initCalcs object with the calculations.
     *
     * After this is completed each calculated property is copied to this item
     *
     * Finally, the observers of this item are notified that the calculations have changed
     */
    public updateArrayCalcs(): void {
        let initCalcs = {
            assigns: 0,
            completedAssigns: 0,
            courses: 0,
            completedCourses: 0,
            credits: 0,
            completedCredits: 0,
            terms: this.array.length,
            completedTerms: 0,
            gradePoints: 0,
            completedGradePoints: 0,
            minGradePoints: 0,
            maxGradePoints: 0,
            totalPoints: 0,
            minPoints: 0,
            maxPoints: 0,
            percentComplete: 0
        };
        let toBeDistributed: IToBeDistributed = {
            percentCompleteSum: 0
        };
        let calculations = this.array.reduce(User.addItemCalcs.bind(this, toBeDistributed), initCalcs);
        calculations.percentComplete = toBeDistributed.percentCompleteSum / calculations.terms;
        for (let prop in calculations) {
            if (calculations.hasOwnProperty(prop)) {
                this[prop] = calculations[prop];
            }
        }
        this.notify();
    }

    // </editor-fold>

    // <editor-fold desc="NBObserver Methods">
    /**
     * Observer method for the User
     *
     * @description
     * Whenever something that this EduItem is observing changes this method is called. This will usually be the _array
     * property.
     *
     * This method will kick off an update of the calculations by calling [[User.updateArrayCalcs]]
     * @param update
     */
    public changed<EduItem>(update: EduItem | NBObservableArray<EduItem>): void {
        this.updateArrayCalcs();
        super.changed(update);
    }

    // </editor-fold>

    // <editor-fold desc="Object Methods">
    /**
     * Override the EduItem toObj to extend the default object with the additional
     *   'name', 'email', 'pass' and 'lastLoggedIn' fields
     * @returns {any}
     */
    public toObj(): UserPOJO {
        let obj = super.toObj() as UserPOJO;
        obj.name = this._name ? this._name.toObj() : new NBName();
        obj.email = this.email;
        obj.pass = this.pass;
        obj.lastLoggedIn = this.lastLoggedIn;
        return obj;
    }

    /**
     * Override the EduItem mergeFrom to copy the additional properties
     *   '_name', 'email', 'pass', 'lastLoggedIn'  and the array
     * @param obj
     */
    public mergeFrom(obj: any): this {
        super.mergeFrom(obj);
        this._name = new NBName(obj.name) || this._name;
        this.email = obj.email || this.email;
        this.pass = obj.pass || this.pass;
        this.lastLoggedIn = obj.lastLoggedIn || this.lastLoggedIn;
        if (obj.array) {
            for (let term of obj.array) {
                let t = term;
                if (!isEduItem(t)) {
                    t = NBObservableProxy(new Term(t)) as EduItem;
                }
                this.addOrMergeItem(t);
            }
        }
        this.changed(this);
        return this;
    }

    protected inheritFrom(parent: EduItem): void {
        console.error('Users cannot have parent items');
    }
    // </editor-fold>
}
