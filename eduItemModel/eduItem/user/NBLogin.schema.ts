/**
 * Created by Nick on 1/8/2017.
 */

import {Schema} from 'mongoose';
import * as moment from 'moment';

// todo: put in seperate file
export let NBLoginSchema = new Schema(
    {
        time       : {
            type: Number, default: function (): number {
                return moment.utc().valueOf();
            }, required: true
        },
        message: {type: String, required: false},
        success    : {type: Boolean, required: true},
        activity   : {type: String, trim: true},
        ipAddress  : {
            type: String, trim: true, default: function (): string {
                return '';
            }
        }
    }
);
