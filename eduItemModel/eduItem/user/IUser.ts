/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 9/4/2016.
 */
import {NBObservableArray} from '@nb/nbobservable';
import {NBName} from '@nb/nbname';
import {EduItemPOJO, isEduItemPOJO} from '../IEduItem';
import {User} from './user';
import {Term} from '../term/';

export interface UserPOJO extends EduItemPOJO {
    array: Term[];
    name: NBName | String;
    email: string;
    pass: string;
    lastLoggedIn: number;
}

export function isUserPOJO(object: any): object is UserPOJO {
    return isEduItemPOJO(object) &&
        'name' in object &&
        'email' in object &&
        'pass' in object &&
        'lastLoggedIn' in object;
}
export function isUser(object: any): object is User {
    return isUserPOJO(object) && object instanceof User;
}
