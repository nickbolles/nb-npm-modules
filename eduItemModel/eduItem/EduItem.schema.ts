/**
 * Created by Nick on 11/14/2016.
 */

import { Document, Schema, model } from 'mongoose';
import * as moment from 'moment';
import {EduItem} from './EduItem';
import * as uuid from 'uuid';

// todo: disable ID getter
// http://mongoosejs.com/docs/guide.html#id
// todo: set indexes
// http://mongoosejs.com/docs/guide.html#indexes

// function createMethodsFromProto(schema: Schema, proto: Object, names: string[]): Schema {
//     names.forEach((funcName) => {
//         let func = Object.getOwnPropertyDescriptor(proto, funcName);
//         if (isFunction(func.value)) {
//             schema.method(funcName, func.value);
//         } else if (isFunction(func.get))
//     });
// }

export function EduItemSchemeFactory(): any {
    return {
        id: { type: String, required: true, default: () => {return uuid(); } },
        label: {type: String, required: true},
        completed: {type: Boolean, required: true, default: false},
        createdTS: {type: Number, required: true, default: () => {return moment.utc().valueOf(); }},
        modifiedTS: {type: Number, required: true, default: () => {return moment.utc().valueOf(); }},
        groupName: {type: String, required: true, default: 'ungrouped'},
        array: { type: [Schema.Types.Mixed], default: null},
    // todo: create group scheme
        groups: {type: [Object]},
        tags: {type: [String], index: true},
        notes: {type: String, required: true, default: '', trim: true},
        _version: {type: Number, required: true, default: EduItem._version},
        EduItemType: {type: String, required: true}
    };
}

/*function EduItemSchemaMethods(schema: Schema): Schema {
    let funcNames = ['addItems', 'addOrMergeItem', 'getItemIndex', 'deleteItems'];
    return createMethodsFromProto(schema, EduItem, funcNames);
}*/
