/**
 * The module for the EduItem data model
 *
 * @module EduItemModel
 * @preferred
 */
/**
 * Created by Nicholas on 6/26/2016.
 */
import * as moment from 'moment';
import {NBUtils} from '@nb/nbutils';
import * as uuid from 'uuid';
import {NBObservableArray, NBObservable, NBObserver} from '@nb/nbobservable';
import {isIGradedItem, IGradedItemEnumerableProps} from '../grade/';
import {isIGPAItem, IGPAItemEnumerableProps} from '../gpa/';
import {INBGroupArray} from '../group/';
import {EduItemFactory} from '../eduItemFactory';
import {IEduItemTypeDef} from './IEduItemTypeDef';
import {EduItemPOJO, isEduItem} from './IEduItem';


export class EduItemObservable<T extends EduItem> extends NBObservable<T> {
}

/* tslint:disable:member-ordering */
/**
 * Abstract EduItem class that every EduItem type should extend
 */
export abstract class EduItem extends EduItemObservable<EduItem>
                                implements IEduItemTypeDef,
                                            EduItemPOJO,
                                            NBObserver<EduItem | NBObservableArray<EduItem>> {
    public static readonly _version: number = 1;

    /**
     * The array of sub-items for this EduItem
     *
     * @description
     * Because courses have Assignments, this will be an array of assignments
     *
     * @type {NBObservableArray<Assign>}
     */
    public name: string;

    /**
     * The name of the type of this EduItem
     * @type {string}
     */
    public EduItemType: string;

    /**
     * Angular haskey
     *
     * @description
     * This variable is needed by angular in order to keep track of objects at runtime.
     * Not specifying this can cause some issues at runtime.
     * @type {string}
     */
    public $$hashkey: string = ''; // Angular Requirement

    /**
     * The revision of the class that this is
     *
     * @description
     * The revision is used in deserializing the saved EduItem. If the _version is different
     * then the parser uses an [[EduItemUpgradeProxy]] and the upgrade maps to update the old
     * properties to the new properties.
     *
     * @type {number}
     * @private
     */
    public readonly _version: number = EduItem._version;

    // <editor-fold desc="Instance Properties">
    /**
     * EduItem Id
     *
     * @description
     * A unique
     * [RFC4122, version 4 ID](https://en.wikipedia.org/wiki/Universally_unique_identifier#Version_4_.28random.29)
     * for the EduItem
     *
     * @type {string}
     */
    public id: string = uuid();

    // <editor-fold desc="label">
    /**
     * The internal label
     *
     * @description
     * The raw string for the label
     *
     * @type {string}
     * @private
     */
    private _label: string = '';
    /**
     * Get the label for the EduItem
     * @description
     * The label is normalized to [Title Case](https://en.wikipedia.org/wiki/Letter_case#Case_styles)
     *
     * @returns {string}
     */
    get label(): string {
        return this._label;
    }
    set label(newStr: string) {
        if (!newStr) {
            this._label = '';
            return;
        }
        this._label = newStr.replace(/\b\w/g, function(l: string): string { return l.toUpperCase(); });
    }
    // </editor-fold>

    /**
     * State of the EduItem
     *
     * @type {boolean}
     */
    public completed: boolean = false;

    // todo: convert to groupId
    /**
     * The name of the group the EduItem belongs to
     *
     * @type {string}
     */
    public groupName: string = 'ungrouped';

    // todo: convert to observable array
    /**
     * The groups in this EduItem
     *
     * @description
     * An array of group metadata. Items in the array will have a [[EduItem.groupName]] property
     * that corresponds to an entry in the groups array.
     *
     * @type {Array}
     */
    public groups: INBGroupArray = [];

    /**
     * Tags for the EduItem
     *
     * @description
     * Tags is an array of strings that the EduItem has some sort of relation to.
     * These can be used to search for EduItems that are related.
     *
     * @type {Array}
     */
    public tags: string[] = [];

    /**
     * User notes on the EduItem
     * @type {string}
     */
    public notes: string = '';

    /**
     * A Moment instance of when the EduItem was created
     * The shortcut of [[EduItem.createdTS]] is a getter/setter combo to ensure correct Unix timestamp conversion
     *
     * @type {moment.Moment}
     */
    public created: moment.Moment = moment.utc();
    // <editor-fold desc="CreatedTS">
    /**
     * Get unix timestamp that the item was created
     *
     * @note
     * This is equal to [[EduItem.created]].valueOf();
     * @returns {number}
     */
    get createdTS(): number {
        return this.created ? this.created.valueOf() : null;
    }
    set createdTS(val: number) {
        this.created = moment.utc(val);
    }

    // </editor-fold>

    /**
     * A Moment instance of when the EduItem was last modified
     * The shortcut of [[EduItem.modifiedTS]] is a getter/setter combo to ensure correct Unix timestamp conversion
     * @type {moment.Moment}
     */
    public modified: moment.Moment = moment.utc();
    // <editor-fold desc="ModifiedTS">
    /**
     * Get unix timestamp that the item was modified
     *
     * @note
     * This is equal to [[EduItem.modified]].valueOf();
     * @returns {number}
     */
    get modifiedTS(): number {
        return this.modified ? this.modified.valueOf() : null;
    }

    set modifiedTS(val: number) {
        this.modified = moment.utc(val);
    }

    // </editor-fold>

    // <editor-fold desc="array">
    private _array: NBObservableArray<EduItem>;
    /**
     * An Observable array of EduItems.
     * The getter simply returns the private _array property.
     * The setter subscribes from the old array and subscribes to the new array
     *
     * @returns {NBObservableArray<EduItem>}
     */
    get array(): NBObservableArray<EduItem> {
        return this._array;
    }

    set array(newVal: NBObservableArray<EduItem>) {
        if (this._array) {
            this._array.unsubscribe(this);
        }
        if (!newVal) {
            this._array = newVal as any;
            return;
        }
        this._array = newVal;
        this._array.subscribe(this);
        if ('updateArrayCalcs' in this) {
            (this as any).updateArrayCalcs();
        }
    }

    // </editor-fold>

    // </editor-fold>

    // <editor-fold desc="Constructor">
    /**
     * Constructor
     *
     * @param obj
     */
    constructor(obj?: EduItemPOJO) {
        super();
    }

    // </editor-fold>

    // <editor-fold desc="Methods">
    // <editor-fold desc="Add/Remove Items">
    /**
     * Add the items to the array.
     *
     * @description
     * This passes off to addOrMergeItem, which should safely add or merge the items.
     *
     * @param items
     * @returns {EduItem[]} An array of the items added or merged. This could be different
     *                      then the one that was passed in if there were existing items with the same id's
     */
    public addItems(...items: EduItem[]): EduItem[] {
        return items.map(this.addOrMergeItem);
    }

    /**
     * Add an item to the array without overwriting an existing one
     *
     * Safely add the item to the array. This should avoid any duplicate items because if it finds
     * a duplicate it will instead merge the item to the found item
     *
     * @param item
     * @return {EduItem}
     */
    // todo: improve the merging logic
    public addOrMergeItem(item: EduItem): EduItem {
        // todo: this could be a major problem with id conflicts
        //      (although there are probly other issues if there are id conflicts)
        // Make sure that this item isn't in the array already
        let idx = this.getItemIndex(item.id.toString());
        if (idx === -1) {
            this.array.push(item);
        } else {
            if (isEduItem(this._array[idx])) {
                this._array[idx].mergeFrom(item);
                item = this._array[idx];
            }
        }
        return item;
    }

    /**
     * Delete the items from the array
     *
     * This passes of to [[EduItem._array]].splice, which should notify this instance of the deletions
     *
     * @param ids
     * @returns {boolean} Whether the operation was successful. True if all deletions were successful
     */
    public deleteItems(...ids: Array<string | EduItem>): boolean {
        let success = true;
        ids.forEach((id: string | EduItem) => {
            let idx = this.getItemIndex(id);
            if (idx !== -1) {
                this._array.splice(idx, 1);
            } else {
                success = false;
            }
        });
        return success;
    }

    // </editor-fold>

    // <editor-fold desc="Finding Items">
    /**
     * Get the index of the item.
     *
     * @param id Either the ID of the item, or the item itself
     * @returns {number} The index of the item, -1 if it doesn't exist
     */
    public getItemIndex(id: string | EduItem): number {
        if (!this._array) {
            return -1;
        } else if (isEduItem(id)) {
            return this._array.indexOf(id);
        } else {
            return NBUtils.arrayObjectIndexOf((this._array as Array<EduItem>), 'id', id);
        }
    }

    /**
     * Find an item recursively. This will check the array for the item, then check each child
     *
     * @param id The ID of the item
     * @returns {EduItem|void} The EduItem, or undefined
     */
    public findItem(id: string): EduItem | void {
        let idx = this.getItemIndex(id);

        if (!this._array) {
            return null;
        } else if (idx !== -1) {
            return this._array[idx];
        }
        let result: EduItem | void = null;
        this._array.every((val: EduItem) => {
            result = val.findItem(id);
            return !result; // As long as there isn't a result continue
        });
        return result;
    }

    // </editor-fold>

    // <editor-fold desc="Helper Methods">

    /**
     * A place to do things that Typescript wont let you do in the constructor
     *
     * @param obj The object passed into the constructor
     * @param parent
     */
    protected postInit(obj: any, parent?: EduItem): void {
        if (obj) {
            this.mergeFrom(obj);
        }
        if (parent) {
            this.inheritFrom(parent);
        }
        if (this._array) {
            this._array.subscribe(this);
        }
        let t = this as any;
        if (t.updateArrayCalcs) {
            t.updateArrayCalcs();
        }
    }

    /**
     * Function to get the constructor of this instance
     * This is extremely useful when needing to access the static properties of the constructor
     *
     * @returns {Function}
     */
    public getClass(): any {
        return this.constructor;
    }

    /**
     * Update the modified timestamp to the current time
     */
    public bumpModified(): void {
        this.modified = moment.utc();
    }

    /**
     * Get an array of properties that are calculated for this eduItem type
     *
     * @returns {any}
     */
    public calculatedProps(): string[] {
        if (isIGradedItem(this)) {
            // todo: replace angular.copy
            return NBUtils.copy<string[]>(IGradedItemEnumerableProps);
        } else if (isIGPAItem(this)) {
            return NBUtils.copy<string[]>(IGPAItemEnumerableProps);
        }
    }
    // </editor-fold>
    // </editor-fold>

    // <editor-fold desc="NBObserver Methods">
    /**
     * Function that is called when the observed items change
     * This will be called when the array of items changes.
     * This should update the modified timestamp and propagate the changes to any of this EduItems listeners
     * @param update
     */
    public changed<EduItem>(update: EduItem | NBObservableArray<EduItem>): void {
        this.bumpModified();
        this.notify();
    }

    // </editor-fold>

    // <editor-fold desc="Object Methods">
    /**
     * Convert the EduItem to an object, then serialize it to JSON
     * Because the toObj function is called to convert this EduItem to a POJO this will most-likely never need to be
     * overridden because the toObj function handles adding additional properties of subClasses to the POJO
     * representation
     * @returns {string}
     */
    public toJSON(): EduItemPOJO {
        return this.toObj();
    }

    /**
     * Create a POJO from this EduItem
     * This is essential to serializing to JSON
     * Each subclass can extend this function (by overriding and calling super.toObj() and adding to the returned
     * object) to include each of their custom properties
     *
     * @returns {EduItemPOJO}
     */
    public toObj(): EduItemPOJO {
        return {
            id: this.id,
            label: this.label,
            completed: this.completed,
            createdTS: this.createdTS,
            modifiedTS: this.modifiedTS,
            groupName: this.groupName,
            array: this.array ? this.array.toJSON() : this.array, // todo: ensure proper serialization
            groups: this.groups,
            tags: this.tags,
            notes: this.notes,
            // todo: Have an app version for compatibility
            _version: EduItem._version,
            EduItemType: this.getClass().name
        };
    }

    /**
     * Merge the properties from a POJO, or another EduItem to this EduItem
     * @param obj
     */
    // todo: this should call this.changed()
    public mergeFrom(obj: any): this {
        this.id = obj.id || this.id;
        this.label = obj.label || this.label;
        this.completed = obj.completed || this.completed;
        this.createdTS = obj.createdTS || this.createdTS;
        this.modifiedTS = obj.modifiedTS || this.modifiedTS;
        this.groupName = obj.groupName || this.groupName;
        this.groups = obj.groups || this.groups;
        this.tags = obj.tags || this.tags;
        this.notes = obj.notes || this.notes;
        return this;
    }

    /**
     * This method takes its name from the Node class, so libraries (like angular) will use this function to
     * copy the EduItem
     */
    public cloneNode(): EduItem {
        return EduItemFactory.newOfType(this.getClass(), this.toObj());
    }

    /**
     * Define the default toString for all EduItems
     * This makes it much easier to print out an item and get a useful description, not a billion sub items
     * @returns {string}
     */
    public toString(): string {
        let obj = this.toObj() as any;
        if (this.getClass().arrayType) {
            obj.array = [`...${this.array.length} ${this.getClass().arrayType.name}` +
                          `${this.array.length !== 1 ? 's' : ''}...`];
        }
        return JSON.stringify(obj);
    }

    /**
     * Inherit properties from the EduItem that this item belongs to
     * @param parent
     */
    protected inheritFrom(parent: EduItem): void {
        this.completed = parent.completed;
    }

    // </editor-fold>
}
