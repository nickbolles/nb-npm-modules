/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 7/28/2016.
 */
import * as moment from 'moment';
import {NBProxyUpgradeMap, NBProxyTransform} from '@nb/nbproxy';
import {NBObservableArray} from '@nb/nbobservable';
import {Course} from '../course/course';
import {EduItemFactory} from '../../eduItemFactory';
import {EduItemPOJO} from '../IEduItem';

export const TermUpgradeMap: NBProxyUpgradeMap = [
    {
        $$hashkey: function(target: any, value: any): NBProxyTransform {
            return {
                prop: undefined,
                val: undefined,
                message: `'$$hashkey' does not exist on updated data model, removing`
            };
        },
        _id: function(target: any, value: any): NBProxyTransform {
            return {
                prop: undefined,
                val: undefined,
                message: `'_id' does not exist on updated data model, remvoing`
            };
        },
        termName: function (target: any, value: any): NBProxyTransform {
            return {
                prop: 'label',
                val: value,
                message: `'termName' with value ${value} converted to 'label'`
            };
        },
        termSDate: function (target: any, value: any): NBProxyTransform {
            return {
                prop: 'startDateTS',
                val: moment.utc(value),
                message: `'termSDate' with value ${value} converted to 'startDateTS'`
            };
        },
        termEDate: function (target: any, value: any): NBProxyTransform {
            return {
                prop: 'endDateTS',
                val: moment.utc(value),
                message: `'termSDate' with value ${value} converted to 'endDateTS'`
            };
        },
        courses: function (target: any, value: any): NBProxyTransform {
            let array = new NBObservableArray<Course>();
            value.forEach((val: EduItemPOJO) => {
                array.push(EduItemFactory.fromObj(val) as Course);
            });
            return {
                prop: 'array',
                val: array,
                message: `'courses' property converted to NBObservableArray<Course>`
            };
        },
        termId: function (target: any, value: any): NBProxyTransform {
            return {
                prop: 'id',
                val: value,
                message: `'termId' with value ${value} converted to 'id'`
            };
        },
        created: function (target: any, value: any): NBProxyTransform {
            let prop = 'created';
            let message = `'created' on Term is not a number, keeping as 'created' property`;
            if (typeof value === 'number') {
                prop = 'createdTS';
                message = `'created' on Term with value ${value} converted to 'createdTS'`;
            }
            return {
                prop: prop,
                val: value,
                message: message
            };
        },
        modified: function (target: any, value: any): NBProxyTransform {
            let prop = 'modified';
            let message = `'modified' on Term is not a number, keeping as 'modified' property`;
            if (typeof value === 'number') {
                prop = 'modifiedTS';
                message = `'modified' on Term with value ${value} converted to 'modifiedTS'`;
            }
            return {
                prop: prop,
                val: value,
                message: message
            };
        }
    },
    {
        array: function (target: any, value: any): NBProxyTransform {
            let array = new NBObservableArray<Course>();
            value.forEach((val: EduItemPOJO) => {
                array.push(EduItemFactory.fromObj(val) as Course);
            });
            return {
                prop: 'array',
                val: array,
                message: `'array' Property converted into NBObservableArray<Term> from raw array`
            };
        }
    }
];
