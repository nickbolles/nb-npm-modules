import {Document, Schema} from 'mongoose';
import * as moment from 'moment';
import {NBEnumUtils} from '@nb/nbutils';
import {EduItemSchemeFactory} from '../EduItem.schema';
import {CourseSchema} from '../course/Course.schema';
import {Term, TermType} from './term';
// import {NBEnumUtils} from '../../../nbCommon/NBUtils/NBEnumUtils';
/**
 * Created by Nick on 11/23/2016.
 */

let emailRegex = new RegExp('[A-Z0-9a-z.!#$%&\'*+-/=?^_`{|}~]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}');
let emailMatch = [
    emailRegex,
    '({VALUE}) is not a valid Email Address'
];
export function TermSchemaFactory(): any {
    let obj = EduItemSchemeFactory();
    obj.EduItemType.default = Term.name;
    obj.array = { type: [CourseSchema], required: true, default: []};
    obj.startDateTS = {type: Number, default: () => moment.utc().valueOf()} ;
    obj.endDateTS = {type: Number, default: () => moment.utc().valueOf()};
    obj.termType = {type: String, enum: NBEnumUtils.getNames(TermType)};

    return obj;
}

export let TermSchema = new Schema(TermSchemaFactory());

export interface TermDocument extends Term, Document {
    toJSON(): any;
}

// export const Terms = model<TermDocument>('User', TermSchema);
