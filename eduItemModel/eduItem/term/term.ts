/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 7/6/2016.
 */
import {NBObserver, NBObservableArray, NBObservableProxy} from '@nb/nbobservable';
import * as moment from 'moment';
import {IGPAItem, GPAScale} from '../../gpa/';
import {EduItem, isEduItem, IEduItemTypeDef} from '../';
import {TermPOJO} from './ITerm';
import {User} from '../user/';
import {Course} from '../course/';

interface IToBeDistributed {
    completedWeight: number;
}

/**
 * Enum for termTypes
 */
export enum TermType {
    Semester,
    Trimester,
    Quarter,
    Other
}

export class Term extends EduItem implements IEduItemTypeDef, IGPAItem, TermPOJO,
                                                NBObserver<EduItem | NBObservableArray<EduItem>> {

    // <editor-fold desc="Static Properties">
    public static name: string;
    public static dispName: string = 'Term';
    public static arrayType: IEduItemTypeDef = Course;
    public static stateName: string = 'term';
    // </editor-fold>

    // <editor-fold desc="Static Methods">
    /**
     * Function to add the calculations from an item in the array to the calculations object
     *
     * @param toBeDistributed
     * @param calcs
     * @param course
     * @returns {IGPAItem}
     */
    private static addItemCalcs(toBeDistributed: IToBeDistributed, calcs: IGPAItem, course: Course): IGPAItem {
        calcs.assigns += course.assigns;
        calcs.completedAssigns += course.completedAssigns;
        // While completed is a boolean it is converted to 1 or 0 because this is an arithmetic operation
        calcs.completedCourses += Number(course.completed);
        calcs.credits += course.credits;
        calcs.completedCredits += course.completed && course.credits;
        calcs.gradePoints += course.grade.gradePoints * course.credits;
        calcs.completedGradePoints += course.completed && course.grade.gradePoints * course.credits;
        calcs.minGradePoints += course.gradeScale.getGrade(course.minPercentage).gradePoints * course.credits;
        calcs.maxGradePoints += course.gradeScale.getGrade(course.maxPercentage).gradePoints * course.credits;
        calcs.totalPoints += course.totalPoints;
        calcs.minPoints += course.minPercentage * course.totalPoints;
        calcs.maxPoints += course.maxPercentage * course.totalPoints;
        // Sum up a weighted average of the completed weight
        toBeDistributed.completedWeight += course.credits * course.completedWeight;
        return calcs;
    }
    // </editor-fold>

    // <editor-fold desc="Instance Properties">

    // <editor-fold desc="EduItem Properties">
    public array: NBObservableArray<Course> = new NBObservableArray<Course>();
    public EduItemType: string = Term.name;
    // </editor-fold>

    // <editor-fold desc="IGPAItem Properties">
    public assigns: number;
    public completedAssigns: number;
    public courses: number;
    public completedCourses: number;
    public credits: number;
    public completedCredits: number;
    public gradePoints: number;
    public completedGradePoints: number;
    public minGradePoints: number;
    public maxGradePoints: number;
    public totalPoints: number;
    public minPoints: number;
    public maxPoints: number;
    public percentComplete: number;

    // <editor-fold desc="gpa">

    get gpa(): number {
        return this.gradePoints / this.credits;
    }

    get minGpa(): number {
        return this.minGradePoints / this.credits;
    }

    get maxGpa(): number {
        return this.maxGradePoints / this.credits;
    }

    // </editor-fold>

    public gpaScale: GPAScale = GPAScale.defaultScale;
    // </editor-fold>

    /**
     * Start date of the Term
     * @type {moment.Moment}
     */
    public startDate: moment.Moment = moment.utc();

    /**
     * Unix timestamp of start date
     * @returns {number}
     */
    // <editor-fold desc="startDateTS">
    get startDateTS(): number {
        return this.startDate ? this.startDate.valueOf() : null;
    }

    set startDateTS(val: number) {
        if (typeof val !== 'number') {
            let mom = moment(val);
            if (moment.isMoment(mom) && mom.isValid()) {
                this.startDate = mom;
                return;
            }
        }
        this.startDate = val ? moment.utc(val) : null;
    }

    // </editor-fold>
    /**
     * End date of the Term
     * @type {moment.Moment}
     */
    public endDate: moment.Moment = moment.utc().add(16, 'w');

    /**
     * Unix timestamp of the end date
     * @returns {number}
     */
    // <editor-fold desc="endDateTS">
    get endDateTS(): number {
        return this.endDate ? this.endDate.valueOf() : null;
    }

    set endDateTS(val: number) {
        if (typeof val !== 'number') {
            let mom = moment(val);
            if (moment.isMoment(mom) && mom.isValid()) {
                this.endDate = mom;
                return;
            }
        }
        this.endDate = val ? moment.utc(val) : null;
    }

    // </editor-fold>
    /**
     * Type of the Term
     * @type {TermType}
     */
    public termType: TermType = TermType.Semester;
    // </editor-fold>

    // <editor-fold desc="Constructor">
    constructor(obj?: any, parent?: User) {
        super(obj);
        this.postInit(obj, parent);
    }

    // </editor-fold>

    // <editor-fold desc="Calculation Methods">
    /**
     * Method to update the calculations for this object
     *
     * @description
     * This method loops through every item in the array and calls [[Term.addItemCalcs]], which will increment
     * the initCalcs object with the calculations.
     *
     * After this is completed each calculated property is copied to this item
     *
     * Finally, the observers of this item are notified that the calculations have changed
     */
    public updateArrayCalcs(): void {
        let initCalcs = {
            assigns: 0,
            completedAssigns: 0,
            courses: this.array.length,
            completedCourses: 0,
            credits: 0,
            completedCredits: 0,
            gradePoints: 0,
            completedGradePoints: 0,
            minGradePoints: 0,
            maxGradePoints: 0,
            totalPoints: 0,
            minPoints: 0,
            maxPoints: 0,
            percentComplete: 0
        };
        let toBeDistributed = {
            completedWeight: 0
        };
        let calculations = this.array.reduce(Term.addItemCalcs.bind(this, toBeDistributed), initCalcs);
        calculations.percentComplete = toBeDistributed.completedWeight / calculations.courses;
        for (let prop in calculations) {
            if (calculations.hasOwnProperty(prop)) {
                this[prop] = calculations[prop];
            }
        }
        this.notify();
    }
    // </editor-fold>

    // <editor-fold desc="NBObserver Methods">
    /**
     * Observer method for the Course
     *
     * @description
     * Whenever something that this Course is observing changes this method is called. This will usually be the _array
     * property.
     *
     * This method will kick off an update of the calculations by calling [[User.updateArrayCalcs]]
     * @param update
     */
    public changed<EduItem>(update: EduItem | NBObservableArray<EduItem>): void {
        this.updateArrayCalcs();
        super.changed(update);
    }
    // </editor-fold>

    // <editor-fold desc="Object Methods">

    /**
     * Override the EduItem toObj to extend the default object with the additional
     *   'startDateTS', 'endDateTS' and 'termType' fields
     *
     * @returns {any}
     */
    public toObj(): TermPOJO {
        let obj = super.toObj() as TermPOJO;
        obj.startDateTS = this.startDate ? this.startDate.valueOf() : null;
        obj.endDateTS = this.endDate ? this.endDate.valueOf() : null;
        obj.termType = this.termType;
        return obj;
    }

    /**
     * Override the EduItem mergeFrom to copy the additional
     *   'startDateTS', 'endDateTS', 'termType' and the array fields
     * @param obj
     */
    public mergeFrom(obj: any): this {
        super.mergeFrom(obj);
        this.startDateTS = obj.startDateTS || this.startDateTS;
        this.endDateTS = obj.endDateTS || this.endDateTS;
        this.termType = obj.termType || this.termType;
        if (obj.array) {
            for (let course of obj.array) {
                let t = course;
                if (!isEduItem(t)) {
                    t = NBObservableProxy(new Course(t)) as EduItem;
                }
                this.addOrMergeItem(t);
            }
        }
        this.changed(this);
        return this;
    }

    /** Inherit the gpascale property of the parent
     * @param parent
     */
    protected inheritFrom(parent: User): void {
        super.inheritFrom(parent);
        this.gpaScale = parent.gpaScale || GPAScale.defaultScale;
    }

    // </editor-fold>
}
