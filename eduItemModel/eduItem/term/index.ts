import {Term} from './term';

// Export the interface and the class
export * from './ITerm';
export * from './term';
// Export the upgrade map for the Term class
export * from './termUpgradeMap';

export default Term;