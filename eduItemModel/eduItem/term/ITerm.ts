/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 9/4/2016.
 */
import {EduItemPOJO, isEduItemPOJO} from '../IEduItem';
import {TermType, Term} from './term';
import {Course} from '../course/course';
import {NBObservableArray} from '@nb/nbObservable';

export interface TermPOJO extends EduItemPOJO {
    array: NBObservableArray<Course>;
    startDateTS: number;
    endDateTS: number;
    termType: TermType;
}

export function isTermPOJO(object: any): object is TermPOJO {
    return isEduItemPOJO(object) &&
        'startDateTS' in object &&
        'endDateTS' in object &&
        'termType' in object;
}
export function isTerm(object: any): object is Term {
    return isTermPOJO(object) && object instanceof Term;
}
