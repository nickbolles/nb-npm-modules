/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 7/30/2016.
 */
import {IEduItemTypeDef, User, Term, Course, Assign} from './eduItem/';

// Use a static class instead of an enum because it isn't broken like enums are...
// typing actually works this way and it is functionally the same code
export type EEduItemTypes =
    'User' |
    'Term' |
    'Course' |
    'Assign';

export class EduItemType {
    public static User: IEduItemTypeDef = User;
    public static Term: IEduItemTypeDef = Term;
    public static Course: IEduItemTypeDef = Course;
    public static Assign: IEduItemTypeDef = Assign;
}
