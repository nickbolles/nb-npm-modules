/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 9/4/2016.
 */
import {GradeScale} from './GradeScale';

export class Grade {
    constructor(public name: string,
                public description: string = '',
                public low: number = null,
                public high: number = null,
                public gradePoints: number,
                public color: string,
                gradeScale: GradeScale = GradeScale.defaultScale) {
        gradeScale.addGrade(this);
    }

    public toString(): string {
        return `${name} ${this.rangeStr()} ${this.gradePointStr()}`;
    }

    public rangeStr(): string {
        let str = '';
        str += this.low || '<';
        if (this.low && this.high) {
            str += '-';
        }
        str += this.high ? this.high : '>';
        return str;
    }

    public gradePointStr(): string {
        return this.gradePoints + ' GP';
    }
}
