/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 9/4/2016.
 */
import {Grade} from './Grade';

/*
 'Normal' courses	Honors/AP courses
 Grade   Percentage         GPA       Percentage      GPA
 A      90 - 100      3.67 - 4.00    93 - 100    4.5 - 5.0
 B      80 - 89       2.67 - 3.33    85 - 92     3.5 - 4.49
 C      70 - 79       1.67 - 2.33    77 - 84     2.5 - 3.49
 D      60 - 69       1.00 - 1.33    70 - 76     2.0 - 2.49
 F      00 - 59       0.00 - 0.99    0  - 69     0.0 - 1.99
 'gradeScale':[
 {'name':'A', 'low':90,'high':', 'gradePoints':4.0},
 {'name':'B', 'low':80,'high':90, 'gradePoints':3.0},
 {'name':'C', 'low':70,'high':80, 'gradePoints':2.0},
 {'name':'D', 'low':60,'high':70, 'gradePoints':1.0},
 {'name':'F', 'low':0,'high':60, 'gradePoints':0.0}
 ]
 */

/* Notes:
* When creating the grade scale wizard, have a button that will toggle between the standard, and the +- scheme
* Then instruct the user to search for their schools system, provide a link for them to jump to their school google
* search*/
export class GradeScale {
    public static gradeScales: GradeScale[] = [];
    public static defaultScale: GradeScale = new GradeScale('defaultScale');

    public static getGradeScale(name: string): GradeScale | void {
        GradeScale.gradeScales.forEach((value: GradeScale) => {
            if (value.name === name) {
                return value;
            }
        });
    }

    constructor(public name: string, public grades: Grade[] = [],
                public low: number = 0, public high: number = 100) {
        GradeScale.gradeScales.push(this);
    }

    public addGrade(grade: Grade): void {
        if (this.grades.indexOf(grade) === -1) {
            this.grades.push(grade);
            this.sortGrades();
        }
    }

    // todo: make sure that this is correct
    public getGrade(perc: number): Grade {
        for (let i = 0; i < this.grades.length; i++) {
            let low = this.grades[i].low;
            let high = this.grades[i].high;
            if (((perc < high) && (perc >= low)) || (perc < high && !low) || (perc > low && !high)) {
                return this.grades[i];
            }
        }
    }

    public getLowest(): Grade {
        return this.grades[0];
    }

    public getHighest(): Grade {
        return this.grades[this.grades.length - 1];
    }

    // todo: make sure that this is correct
    private sortGrades(): void {
        this.grades.sort((a: Grade, b: Grade) => {
            if (!a.high) {
                return Number.POSITIVE_INFINITY;
            } else if (!b.high) {
                return Number.NEGATIVE_INFINITY;
            } else {
                return a.high - b.high;
            }
        });
    }
}
setTimeout(() => {
    // setup default gradeScale
    new Grade('A', 'Excellent', 90, null, 4.0, '#2E7D32', GradeScale.defaultScale);
    new Grade('B', 'Good', 80, 90, 3.0, '#558B2F', GradeScale.defaultScale);
    new Grade('C', 'Average', 70, 80, 2.0, '#F9A825', GradeScale.defaultScale);
    new Grade('D', 'Below Average', 60, 70, 1.0, '#EF6C00', GradeScale.defaultScale);
    new Grade('F', 'Fail', null, 60, 0.0, '#C62828', GradeScale.defaultScale);
})
