/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 7/19/2015.
 */
// todo:test
// TODO: Document
import * as angular from 'angular';
import {GradeScale} from './GradeScale';
/**
 *
 * @returns {Function}
 * @constructor
 */
function GradeNameFilter(): Function {
  return function (perc: number): string {
    if (perc < 0) {
      return 'N/A';
    }
    return GradeScale.defaultScale.getGrade(perc).name;
  };
}

export let ngModule = angular.module('EduItemModal.Grade.GradeName.Filter', [])
    .filter('gradeName', [
      GradeNameFilter
    ]);
