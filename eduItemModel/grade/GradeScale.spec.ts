import {Grade} from './Grade';
import {GradeScale} from './GradeScale';
/**
 * Created by Nicholas on 7/6/2016.
 */

describe('GradeScale', () => {
    let gradeScale = GradeScale.defaultScale;
    let grade: Grade;
    let grade1: Grade;
    let grade2: Grade;
    let grade3: Grade;
    beforeEach(() => {
        grade = new Grade('A', 'Excellent', 50, null, 4.0, '#2E7D32');
        grade1 = new Grade('B', 'Good', 40, 50, 3.0, '#558B2F');
        grade2 = new Grade('C', 'Average', 30, 40, 2.0, '#F9A825');
        grade3 = new Grade('D', 'Below Average', null, 30, 1.0, '#EF6C00');
    });
    it('should Register Grade', () => {
        expect(gradeScale.grades.length).toBeGreaterThan(0);
    });
    describe('should Sort', () => {
        describe('GradeScale', () => {
            // todo: make sure this is correct
            it('should sort the grades', () => {
                expect(gradeScale.grades[0]).toBe(grade3);
                expect(gradeScale.grades[1]).toBe(grade2);
                expect(gradeScale.grades[2]).toBe(grade1);
                expect(gradeScale.grades[3]).toBe(grade);
            });
        });
    });
    describe('getGrade', () => {
        it('should return the correct grade', () => {
            expect(gradeScale.getGrade(45)).toBe(grade1);
        });
        it('should account for high null', () => {
            expect(gradeScale.getGrade(90)).toBe(grade1);
        });
        it('should account for low null', () => {
            expect(gradeScale.getGrade(20)).toBe(grade1);
        });
    });
    describe('Static Methods', () => {
        it('Register new instances', () => {
            expect(GradeScale.gradeScales.length).toBe(1);
            let newGradeScale = new GradeScale('test');
            expect(GradeScale.gradeScales.length).toBe(2);
        });
        it('Should get by name', () => {
            let newGradeScale = new GradeScale('test');
            expect(GradeScale.getGradeScale('test')).toBe(newGradeScale);
        });
    });
});
