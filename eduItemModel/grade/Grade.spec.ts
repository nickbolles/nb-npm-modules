import {Grade} from './Grade';
import {GradeScale} from './GradeScale';
/**
 * Created by Nicholas on 7/6/2016.
 */

describe('Grade', () => {
    let gradeScale = GradeScale.defaultScale;
    it('should Register Grade', () => {
        let grade = new Grade('A', 'Excellent', 50, 100, 4.0, '#2E7D32');
        expect(gradeScale.grades.length).toBeGreaterThan(0);
    });
    it('should have overloaded toString', () => {
        const str = 'A 50-100 4.0 GP';
        let grade = new Grade('A', 'Excellent', 50, 100, 4.0, '#2E7D32');
        expect('' + grade).toEqual(str);
        expect('' + grade).toEqual(grade.toString());
    });
    describe('should Sort', () => {
        describe('GradeScale', () => {
            let grade1: Grade;
            let grade2: Grade;
            let grade3: Grade;
            let grade4: Grade;
            beforeEach(() => {
                grade1 = new Grade('A', 'Excellent', 50, null, 4.0, '#2E7D32');
                grade2 = new Grade('B', 'Good', 40, 50, 3.0, '#558B2F');
                grade3 = new Grade('C', 'Average', 30, 40, 2.0, '#F9A825');
                grade4 = new Grade('D', 'Below Average', null, 30, 1.0, '#EF6C00');
            });
            // todo: make sure this is correct
            it('should sort the grades', () => {
                expect(gradeScale.grades[2]).toBe(grade2);
                expect(gradeScale.grades[3]).toBe(grade1);
            });
        });
    });
});
