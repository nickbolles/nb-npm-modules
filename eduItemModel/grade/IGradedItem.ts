/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 9/3/2016.
 */
import {GradeScale} from './GradeScale';
import {Grade} from './Grade';

export const IGradedItemEnumerableProps = [
    'assigns',
    'completedAssigns',
    'totalPoints',
    'completedTotalPoints',
    'scoredPoints',
    'completedScoredPoints',
    'weight',
    'completedWeight',
    'earnedWeight',
    'completedEarnedWeight',
    'percentage',
    'minPercentage',
    'maxPercentage',
    'grade',
    'gradeScale'
];

export interface IGradedItem {
    assigns?: number;
    completedAssigns?: number;
    totalPoints: number;
    completedTotalPoints: number;
    scoredPoints: number;
    completedScoredPoints: number;
    weight: number;
    completedWeight: number;
    earnedWeight: number;
    completedEarnedWeight: number;
    percentage: number;
    readonly minPercentage: number;
    readonly maxPercentage: number;
    grade: Grade;
    gradeScale: GradeScale;
    // todo: add minGrade, maxGrade (for use for min/max) GPA calcs
    // todo: add total vs completed
}

export function isIGradedItem(object: any): object is IGradedItem {
    return 'totalPoints' in object &&
        'scoredPoints' in object &&
        'weight' in object &&
        'earnedWeight' in object &&
        'completedWeight' in object &&
        'percentage' in object &&
        'grade' in object &&
        'gradeScale' in object;
}
