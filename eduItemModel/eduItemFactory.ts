/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 6/26/2016.
 */
import {EduItem, User, Term, Course, Assign, EEduItemTypes} from './';
import {EduItemUpgradeProxy} from './upgradeCompatibility/EduItemProxy';
import {isIEduItemTypeDef, IEduItemTypeDef, EduItemPOJO, isEduItem} from './eduItem';
import {NBObservableProxy} from '@nb/nbobservable';

export let EduItemFactory = {
    /**
     * Create an EduItem from JSON
     * This function parses the JSON and passes it to fromObj
     *
     * @param fromStr
     * @returns {EduItem}
     */
    fromJSON: (fromStr: string): EduItem => {
        let obj = JSON.parse(fromStr);
        return EduItemFactory.fromObj(obj);
    },

    /**
     * Create an EduItem from a POJO
     *
     * @param obj
     * @returns {EduItem}
     */
    fromObj: (obj: EduItemPOJO | EduItem | any): EduItem => {
        let getClassName = () => obj.getClass ? obj.getClass().name : null;
        if (isEduItem(obj)) {
            return obj.cloneNode();
        }
        let type = obj.EduItemType || getClassName();
        let rawItem = EduItemFactory.newOfType(type);
        let tempItem = EduItemUpgradeProxy(rawItem);
        // manually merge the properties over
        for (let prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                try {
                    tempItem[prop] = obj[prop];
                } catch (e) {
                    console.log(`EduItemFactory.fromObj: Could not set property ${prop}`, e);
                }
            }
        }
        return rawItem;
    },

    /**
     * Function that takes a name descriptor or a class and an optional object to create the EduItem from
     * Creates the correct item, then it wraps the item in a Proxy, which allows a catch all for properties and
     * allows upgrading of the model
     * @param type
     * @param obj
     * @returns {EduItem}
     */
    newOfType: (type: EEduItemTypes | IEduItemTypeDef, obj?: any, parent?: any): EduItem => {
        switch (isIEduItemTypeDef(type) ? type.name : type) {
            case User.name:
                return NBObservableProxy(new User(obj, parent)) as EduItem;
            case Term.name:
                return NBObservableProxy(new Term(obj, parent)) as EduItem;
            case Course.name:
                return NBObservableProxy(new Course(obj, parent)) as EduItem;
            case Assign.name:
                return NBObservableProxy(new Assign(obj, parent))  as EduItem;
            default:
                throw new TypeError('NewOfType: Type is not an enum of EEduItemTypes');
        }
    },

    /**
     * Get the class of the type of this object
     * @param obj
     * @returns {any}
     */
    classFromObj: (obj: any): IEduItemTypeDef => {
        if (obj.getClass) {
            return obj.getClass();
        }
        switch ( obj.EduItemType ) {
            case User.name:
                return User;
            case Term.name:
                return Term;
            case Course.name:
                return Course;
            case Assign.name:
                return Assign;
            default:
                throw new TypeError('classFromObj: Type is not an enum of EEduItemTypes');
        }
    }
};
