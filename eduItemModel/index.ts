/**
 * The data model for EduItems
 *
 * @module EduItemModel
 */
/**
 * Created by Nick on 11/14/2016.
 */
export * from './eduItem';

export * from './gpa/';

export * from './grade/';

export * from './eduItem/IEduItemTypeDef';

export * from './eduItemFactory';

export * from './EduItemTypes'
