/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 9/4/2016.
 */
import {GPAScale} from './GPAScale';

export class GPA {
    constructor(public name: string,
                public description: string = '',
                public low: number = null,
                public high: number = null,
                public color: string,
                gpaScale: GPAScale = GPAScale.defaultScale) {
        gpaScale.addGrade(this);
    }

    public toString(): string {
        return `${name} ${this.rangeStr()}`;
    }

    public rangeStr(): string {
        let str = '';
        str += this.low || '<';
        if (this.low && this.high) {
            str += '-';
        }
        str += this.high ? this.high : '>';
        return str;
    }
}
