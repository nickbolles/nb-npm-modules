/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 9/3/2016.
 */
import {GPAScale} from './GPAScale';

export const IGPAItemEnumerableProps = [
    'assigns',
    'completedAssigns',
    'courses',
    'completedCourses',
    'terms',
    'completedTerms',
    'credits',
    'completedCredits',
    'gradePoints',
    'completedGradePoints',
    'minGpa',
    'maxGpa',
    'gpa',
    'gpaScale',
    'minGradePoints',
    'maxGradePoints',
    'totalPoints',
    'minPoints',
    'maxPoints',
    'percentComplete'
];

export interface IGPAItem {
    assigns?: number;
    completedAssigns?: number;
    courses?: number;
    completedCourses?: number;
    terms?: number;
    completedTerms?: number;
    credits: number;
    completedCredits: number;
    gradePoints: number;
    completedGradePoints: number;
    readonly minGpa: number;
    readonly maxGpa: number;
    readonly gpa: number;
    gpaScale: GPAScale;
    minGradePoints: number;
    maxGradePoints: number;
    totalPoints: number;
    minPoints: number;
    maxPoints: number;
    percentComplete: number;
}

export function isIGPAItem(object: any): object is IGPAItem {
    return 'credits' in object &&
        'gradePoints' in object &&
        'gpa' in object;
}
