/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 9/4/2016.
 */

/*
 'Normal' courses	Honors/AP courses
 Grade   Percentage         GPA       Percentage      GPA
 A      90 - 100      3.67 - 4.00    93 - 100    4.5 - 5.0
 B      80 - 89       2.67 - 3.33    85 - 92     3.5 - 4.49
 C      70 - 79       1.67 - 2.33    77 - 84     2.5 - 3.49
 D      60 - 69       1.00 - 1.33    70 - 76     2.0 - 2.49
 F      00 - 59       0.00 - 0.99    0  - 69     0.0 - 1.99
 'gradeScale':[
 {'name':'A', 'low':90,'high':', 'gradePoints':4.0},
 {'name':'B', 'low':80,'high':90, 'gradePoints':3.0},
 {'name':'C', 'low':70,'high':80, 'gradePoints':2.0},
 {'name':'D', 'low':60,'high':70, 'gradePoints':1.0},
 {'name':'F', 'low':0,'high':60, 'gradePoints':0.0}
 ]
 */

/* Notes:
* When creating the grade scale wizard, have a button that will toggle between the standard, and the +- scheme
* Then instruct the user to search for their schools system, provide a link for them to jump to their school google
* search*/
import {GPA} from './GPA';
export class GPAScale {
    public static gpaScales: GPAScale[] = [];
    public static defaultScale: GPAScale = new GPAScale('defaultScale');

    public static getGPAScale(name: string): GPAScale | void {
        GPAScale.gpaScales.forEach((value: GPAScale) => {
            if (value.name === name) {
                return value;
            }
        });
    }

    constructor(public name: string, public grades: GPA[] = [],
                public low: number = 0, public high: number = 4.0) {
        GPAScale.gpaScales.push(this);
    }

    public addGrade(grade: GPA): void {
        if (this.grades.indexOf(grade) === -1) {
            this.grades.push(grade);
            this.sortGrades();
        }
    }

    public getGrade(GPA: number): GPA {
        for (let i = 0; i < this.grades.length; i++) {
            let low = this.grades[i].low;
            let high = this.grades[i].high;
            if (((GPA < high) && (GPA >= low)) || (GPA < high && !low) || (GPA > low && !high)) {
                return this.grades[i];
            }
        }
    }

    public getLowest(): GPA {
        return this.grades[0];
    }

    public getHighest(): GPA {
        return this.grades[this.grades.length - 1];
    }

    // todo: make sure that this is correct
    private sortGrades(): void {
        this.grades.sort((a: GPA, b: GPA) => {
            if (!a.high) {
                return Number.POSITIVE_INFINITY;
            } else if (!b.high) {
                return Number.NEGATIVE_INFINITY;
            } else {
                return a.high - b.high;
            }
        });
    }
}

setTimeout(() => {
    // setup default gradescale
    new GPA('4.0', '', 4.0, null, '#2E7D32', GPAScale.defaultScale);
    new GPA('3.8', '', 3.8, 4.0, '#388E3C', GPAScale.defaultScale);
    new GPA('3.5', '', 3.5, 3.8, '#8BC34A', GPAScale.defaultScale);
    new GPA('3.0', '', 3.0, 3.5, '#F9A825', GPAScale.defaultScale);
    new GPA('2.5', '', 2.5, 3.0, '#FFC107', GPAScale.defaultScale);
    new GPA('2.0', '', 2.0, 2.5, '#EF6C00', GPAScale.defaultScale);
    new GPA('1.0', '', 1.0, 2.0, '#F44336', GPAScale.defaultScale);
    new GPA('0', '', null, 1.0, '#C62828', GPAScale.defaultScale);
})
