/**
 * @module EduItemModel
 */
/**
 * Created by Nicholas on 6/28/2016.
 */
// import {IEduItemTypeDef} from '../../eduItemModel/eduItem';

export interface INBGroup {
    name: String;
    id: string;
    weight: Number;
    tags: [String];
    modified: number;
    type: String/*IEduItemTypeDef*/
    ;
}

export interface INBGroupArray {
    [i: number]: INBGroup;
}
