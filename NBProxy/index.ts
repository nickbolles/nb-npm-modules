import {nbProxy} from './NBProxy';

export * from './NBProxy';
export * from './NBProxyUtils';

export default nbProxy;