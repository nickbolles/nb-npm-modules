/**
 * @module NBProxy
 */
/**
 * Created by Nicholas on 8/28/2016.
 */

export interface NBProxyTransform {
    prop: string | number | symbol;
    val: any;
    message: string;
}

export interface NBProxyUpgradeMap {
    [index: number]: {
        [index: string]: (target: any, value: any) => NBProxyTransform
    };
}

export function transformProperty(upgradeMap: NBProxyUpgradeMap, target: any,
                                  name: string | number | symbol, value?: any, logToConsole: boolean = true,
                                  minLog: boolean = false): NBProxyTransform[] {
    let transforms: NBProxyTransform[] = [];
    let prop = name;
    for (let version in upgradeMap) {
        if (!upgradeMap.hasOwnProperty(version)) {continue; }
        // If this property exists in this version of the upgradeMap call the function and continue looking for the prop
        // that is returned from it. Log a warning too
        if (prop in upgradeMap[version]) {
            let result = upgradeMap[version][prop](target, value);
            // continue looking for the next prop
            prop = result.prop;
            value = result.val;
            transforms.push(result);
            if (logToConsole) {
                let str = `Proxy Handler: \r\n\t ${result.message}`;
                if (!minLog) {
                    str += `\r\n\t Object:\r\n\t ${target.toString()}\r\n\t${(new Error() as any).stack}`;
                }
                console.warn(str);
            }
        }
    }
    return transforms;
}

export function merge<T>(upgradeMap: NBProxyUpgradeMap, dest: T, ...src: any[]): T {
    // merge the old object into the new object, taking account for the upgrade map
    src.forEach((srcObj: any) => {
        for (let prop in srcObj) {
            if (srcObj.hasOwnProperty(prop)) {
                transformProperty(upgradeMap, dest, prop, srcObj[prop]);
            }
        }
    });
    return dest;
}
