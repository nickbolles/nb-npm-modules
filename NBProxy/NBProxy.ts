/**
 * @module NBProxy
 */
/**
 * Created by Nicholas on 8/9/2016.
 */

import * as Proxy from 'harmony-proxy';

// note: symbol throws an error with the typescript compiler
//       error TS4023: Exported variable 'defaultHandler' has or is using name 'harmonyProxy.PropertyKey'
//                     from external module "C:/sync/Development/foursee/Modules/NBProxy/node_modules/@types/harmony-proxy/index"
//                     but cannot be named.

function logError(target: Object, name: string | number /*| symbol*/, action: string): void {
    console.error(new Error(`Cannot ${action} property "${name} on object ${target.toString()}, it does not exist`));
}

export const defaultHandler = {
    get: function(target: any, name: string | number /*| symbol*/, reciever: any): void {
        return name in target ? target[name] : logError(target, name, 'get');
    },
    set: function(target: any, name: string | number /*| symbol*/, value: any, reciever: any): boolean {
        if (name in target) {
            target[name]  = value;
            return true;
        }
        logError(target, name, 'set');
        return false;
    }
};

export function nbProxy(fn: any, handler?: Object): any {
    return new Proxy(fn, handler || defaultHandler);
}
