import {nbProxy} from './NBProxy';
import * as Proxy from 'harmony-proxy';
/**
 * Created by Nicholas on 8/9/2016.
 */

describe('nbProxy', () => {
    let obj = {
        foo: 'bar',
        fooBar: function(): string {
            return 'barFoo';
        }
    };
    let proxy: any;
    beforeEach(() => {
        proxy = nbProxy(obj) as any;
    });
    it('should create a Proxy', () => {
        expect(proxy).toBe(jasmine.any(Proxy));
    });
    it('should behave just like an object', () => {
        expect(proxy.foo).toEqual(obj.foo);
        expect(proxy.fooBar()).toEqual(obj.fooBar());
    });
    it('should throw an error when accessing incorrect properties', () => {
        expect(proxy.blah).toThrow();
        expect(proxy.someFunc()).toThrow();
    });
    it('should not be an object', () => {
        expect(proxy).not.toBe(obj);
    });
    it('should be equal to the object', () => {
        expect(proxy).toEqual(obj);
    });
});
